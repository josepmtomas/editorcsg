#include "dialogtransformation.h"
#include "ui_dialogtransformation.h"

DialogTransformation::DialogTransformation(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogTransformation)
{
    ui->setupUi(this);
}

DialogTransformation::DialogTransformation(QWidget *parent, Transformation *transformation, bool *result, bool editing) :
    QDialog(parent),
    ui(new Ui::DialogTransformation)
{
    this->transformation = transformation;
    this->result = result;
	currentTab = 0;

    ui->setupUi(this);

    // Default values
    ui->rotationAxisYSpinBox->setValue(1.0);
    ui->scaleXSpinBox->setValue(1.0);
    ui->scaleYSpinBox->setValue(1.0);
    ui->scaleZSpinBox->setValue(1.0);

    ui->rotationAxisYSpinBox->update();
    ui->scaleXSpinBox->update();
    ui->scaleYSpinBox->update();
    ui->scaleZSpinBox->update();

    if(transformation->type == TransformationType::TRANSLATION)
    {
        ui->translationXSpinBox->setValue(transformation->vectorValue.x);
        ui->translationYSpinBox->setValue(transformation->vectorValue.y);
        ui->translationZSpinBox->setValue(transformation->vectorValue.z);
        ui->translationXSpinBox->update();
        ui->translationYSpinBox->update();
        ui->translationZSpinBox->update();

        ui->tabWidget->setCurrentIndex(0);
    }
    else if(transformation->type == TransformationType::ROTATION)
    {
        ui->rotationAxisXSpinBox->setValue(transformation->vectorValue.x);
        ui->rotationAxisYSpinBox->setValue(transformation->vectorValue.y);
        ui->rotationAxisZSpinBox->setValue(transformation->vectorValue.z);
        ui->rotationAngleSpinBox->setValue(transformation->floatValue);
        ui->rotationAxisXSpinBox->update();
        ui->rotationAxisYSpinBox->update();
        ui->rotationAxisZSpinBox->update();
        ui->rotationAngleSpinBox->update();

        ui->tabWidget->setCurrentIndex(1);
    }
    else if(transformation->type == TransformationType::SCALE)
    {
        ui->scaleXSpinBox->setValue(transformation->vectorValue.x);
        ui->scaleYSpinBox->setValue(transformation->vectorValue.y);
        ui->scaleZSpinBox->setValue(transformation->vectorValue.z);
        ui->scaleXSpinBox->update();
        ui->scaleYSpinBox->update();
        ui->scaleZSpinBox->update();

        ui->tabWidget->setCurrentIndex(2);
    }
}

DialogTransformation::~DialogTransformation()
{
    delete ui;
}

void DialogTransformation::accept()
{
    if(*result == true)
        DialogTransformation::close();
}

void DialogTransformation::on_buttonBox_accepted()
{
    switch(currentTab)
    {
    case 0:
        transformation->vectorValue.x = (float)ui->translationXSpinBox->value();
        transformation->vectorValue.y = (float)ui->translationYSpinBox->value();
        transformation->vectorValue.z = (float)ui->translationZSpinBox->value();

        if(l1Norm(transformation->vectorValue) == 0.0)
        {
            QMessageBox::warning(this, "Transformacion", "La tranlacion no puede ser de valor 0");
            *result = false;
        }
        else
        {
            transformation->type = TransformationType::TRANSLATION;
            *result = true;
            accept();
        }
        break;

    case 1:
        transformation->vectorValue.x = (float)ui->rotationAxisXSpinBox->value();
        transformation->vectorValue.y = (float)ui->rotationAxisYSpinBox->value();
        transformation->vectorValue.z = (float)ui->rotationAxisZSpinBox->value();

        transformation->vectorValue = normalize(transformation->vectorValue);

        if(l1Norm(transformation->vectorValue) == 0.0)
        {
            QMessageBox::warning(this, "Rotación", "La norma del vector del eje no puede ser 0");

            *result = false;
        }
        else
        {
            transformation->floatValue = (float)ui->rotationAngleSpinBox->value();
            if(transformation->floatValue == 0.0)
            {
                QMessageBox::warning(this, "Rotación", "El ángulo de rotación no puede ser 0");
                *result = false;
            }
            else
            {
                transformation->type = TransformationType::ROTATION;
                *result = true;
                accept();
            }
        }

        break;

    case 2:
        transformation->vectorValue.x = (float)ui->scaleXSpinBox->value();
        transformation->vectorValue.y = (float)ui->scaleYSpinBox->value();
        transformation->vectorValue.z = (float)ui->scaleZSpinBox->value();

        if(transformation->vectorValue.x == 0.0 ||
           transformation->vectorValue.y == 0.0 ||
           transformation->vectorValue.z == 0.0)
        {
            QMessageBox::warning(this, "Escalado", "No puede haber un factor de escala 0 en ninguna dirección");
            *result = false;
        }
        else
        {
            transformation->type = TransformationType::SCALE;
            *result = true;
            accept();
        }

        break;

    default:
        break;
    }
}

void DialogTransformation::on_buttonBox_rejected()
{

}



void DialogTransformation::on_tabWidget_currentChanged(int index)
{
    currentTab = index;
}
