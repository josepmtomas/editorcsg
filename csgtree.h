#ifndef CSGTREE_H
#define CSGTREE_H

#include <vector>

#include "csgnode.h"

class CSGTree
{
public:

    std::vector<CSGNode*> nodes;

    int numBoxes;
    int numCylinders;
    int numSpheres;
    int numUnions;
    int numDifferences;
    int numIntersections;

    int numOperations;
    int numPrimitives;

	int IDcount;

    CSGTree();

	int			getTypeSize(NodeType _type);
	void		addNode(CSGNode *_node, bool hasID);
	CSGNode*	getNode(int id);
	CSGNode*	getRoot();
	int			getNodeIndex(int id);
	int			getPrimitiveCount();

    bool nameExists(QString _name);

	void destruct();
};

#endif // CSGTREE_H
