#ifndef TRANSFORMATION_H
#define TRANSFORMATION_H

#include "Algebra.h"
#include <QString>

enum TransformationType
{
    NONE,
    TRANSLATION,
    ROTATION,
    SCALE
};

class Transformation
{
public:

    TransformationType  type;
    vec3    vectorValue;
    float   floatValue;

    Transformation();

    QString getListString();


};

#endif // TRANSFORMATION_H
