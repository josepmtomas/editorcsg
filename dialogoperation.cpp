#include "dialogoperation.h"
#include "ui_dialogoperation.h"

DialogOperation::DialogOperation(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogOperation)
{
    ui->setupUi(this);
}

DialogOperation::DialogOperation(QWidget *parent, CSGTree *_tree, CSGNode *_operation, bool *_result, bool _editing):
    QDialog(parent),
    ui(new Ui::DialogOperation)
{
    tree = _tree;
    node = _operation;
    result = _result;
	editing = _editing;

    ui->setupUi(this);

    updateLists();

    ui->nameLineEdit->setText(node->name);
    ui->nameLineEdit->update();

	if(editing)
	{
		if(node->type == NodeType::OP_UNION)
			setWindowTitle("Editar una unión");
		if(node->type == NodeType::OP_DIFFERENCE)
			setWindowTitle("Editar una diferencia");
		if(node->type == NodeType::OP_INTERSECTION)
			setWindowTitle("Editar una intersección");
	}
	else
	{
		if(node->type == NodeType::OP_UNION)
			setWindowTitle("Crear una unión");
		if(node->type == NodeType::OP_DIFFERENCE)
			setWindowTitle("Crear una diferencia");
		if(node->type == NodeType::OP_INTERSECTION)
			setWindowTitle("Crear una intersección");
	}

	if(node->type == NodeType::OP_UNION)
		ui->label->setPixmap(QString::fromUtf8(":/Images/Resources/iconUnion.png"));
	if(node->type == NodeType::OP_DIFFERENCE)
		ui->label->setPixmap(QString::fromUtf8(":/Images/Resources/iconDifference.png"));
	if(node->type == NodeType::OP_INTERSECTION)
		ui->label->setPixmap(QString::fromUtf8(":/Images/Resources/iconIntersection.png"));


	updateTransformationsList();
}

DialogOperation::~DialogOperation()
{
    delete ui;
}

void DialogOperation::updateLists()
{
    QString name;
	int		id;

    ui->leftListWidget->clear();
    ui->rightListWidget->clear();

	qDebug("Filling lists for node ID = %d", node->id);

	if(editing)
	{
		ui->leftListWidget->addItem("<NONE>");
		ui->rightListWidget->addItem("<NONE>");
		ui->leftListWidget->setCurrentRow(0);
		ui->rightListWidget->setCurrentRow(0);

		for(int i=0; i<tree->nodes.size(); i++)
		{
			name = tree->nodes[i]->getListString();
			id = tree->nodes[i]->id;
			ui->leftListWidget->addItem(name);
			ui->rightListWidget->addItem(name);
			qDebug("\t[%d] Added ID = %d PARENT = %d", i+1, id, tree->nodes[i]->parent);

			if(node->id != id)
			{
				if(tree->nodes[i]->parent != node->id)
				{
					if(tree->nodes[i]->parent >= 0)
					{
						qDebug("\t[%d] Hidden", i+1);
						ui->leftListWidget->setRowHidden(i+1,true);
						ui->rightListWidget->setRowHidden(i+1,true);
					}
				}
				else
				{
					if(node->leftChild)
					{
						if(node->leftChild->id == id)
						{
							qDebug("\t[%d] is LEFT CHILDREN",i+1);
							ui->leftListWidget->setCurrentRow(i+1);
						}
					}

					if(node->rightChild)
					{
						if(node->rightChild->id == id)
						{
							qDebug("\t[%d] is RIGHT CHILDREN",i+1);
							ui->rightListWidget->setCurrentRow(i+1);
						}
					}
				}
			}
			else
			{
				qDebug("\t[%d] Hidden (is the current node)",i+1);
				ui->leftListWidget->setRowHidden(i+1,true);
				ui->rightListWidget->setRowHidden(i+1,true);
			}
		}
	}
	else
	{
		ui->leftListWidget->addItem("<NONE>");
		ui->rightListWidget->addItem("<NONE>");

		for(int i=0; i<tree->nodes.size(); i++)
		{
			name = tree->nodes[i]->getListString();
			ui->leftListWidget->addItem(name);
			ui->rightListWidget->addItem(name);

			if(tree->nodes[i]->parent >= 0)
			{
				ui->leftListWidget->setRowHidden(i+1,true);
				ui->rightListWidget->setRowHidden(i+1,true);
			}
		}

		ui->leftListWidget->setCurrentRow(0);
		ui->rightListWidget->setCurrentRow(0);
	}
}

void DialogOperation::updateTransformationsList()
{
    ui->transformationsListWidget->clear();

    for(int i=0; i<node->transformList.size();i++)
    {
        ui->transformationsListWidget->addItem(node->transformList[i].getListString());
    }
}

void DialogOperation::accept()
{
    if(*result == true)
        DialogOperation::close();
}

void DialogOperation::on_buttonBox_accepted()
{
	int leftIndex = ui->leftListWidget->currentRow();
	int rightIndex = ui->rightListWidget->currentRow();

	if(editing)
	{
		if(ui->nameLineEdit->text().isEmpty())
		{
			QMessageBox::warning(this, "Crear una operación", "La operación debe tener un tombre");
		}
		else if(ui->nameLineEdit->text().compare(node->name) != 0)
		{
			if(tree->nameExists(ui->nameLineEdit->text()))
			{
				QMessageBox::warning(this, "Crear una operación", "El nombre del objeto ya existe");
			}
			else if((leftIndex == 0 && rightIndex == 0) || (leftIndex != rightIndex))
			{
				node->name = ui->nameLineEdit->text();

				if(node->leftChild)
					node->leftChild->parent = -1;

				if(node->rightChild)
					node->rightChild->parent = -1;

				if(leftIndex == 0)
				{
					node->leftChild = NULL;
				}
				else
				{
					node->leftChild = tree->nodes[leftIndex-1];
					node->leftChild->parent = node->id;
				}

				if(rightIndex == 0)
				{
					node->rightChild = NULL;
				}
				else
				{
					node->rightChild = tree->nodes[rightIndex-1];
					node->rightChild->parent = node->id;
				}

				*result = true;
				accept();
			}

		}
		else
		{
			node->name = ui->nameLineEdit->text();

			if(node->leftChild)
				node->leftChild->parent = -1;

			if(node->rightChild)
				node->rightChild->parent = -1;

			if(leftIndex == 0)
			{
				node->leftChild = NULL;
			}
			else
			{
				node->leftChild = tree->nodes[leftIndex-1];
				node->leftChild->parent = node->id;
			}

			if(rightIndex == 0)
			{
				node->rightChild = NULL;
			}
			else
			{
				node->rightChild = tree->nodes[rightIndex-1];
				node->rightChild->parent = node->id;
			}

			*result = true;
			accept();
		}
	}

	else
	{
		if(ui->nameLineEdit->text().isEmpty())
		{
			QMessageBox::warning(this, "Crear una operación", "La operación debe tener un tombre");
		}
		else if(tree->nameExists(ui->nameLineEdit->text()))
		{
			QMessageBox::warning(this, "Crear una operación", "El nombre del objeto ya existe");
		}
		else if((leftIndex == 0 && rightIndex == 0) || (leftIndex != rightIndex))
		{
			*result = true;

			if(leftIndex == 0)
			{
				qDebug("!!! left node is NULL");
				node->leftChild= NULL;
			}
			else
				node->leftChild = tree->nodes[leftIndex-1];

			if(rightIndex == 0)
			{
				qDebug("!!! right node is NULL");
				node->rightChild = NULL;
			}
			else
				node->rightChild = tree->nodes[rightIndex-1];

			node->name = ui->nameLineEdit->text();
			accept();
		}
		else
		{
			QMessageBox::warning(this, "Crear una operación", "Los nodos han de ser distintos");
		}
	}
}

void DialogOperation::on_buttonBox_rejected()
{
    *result = false;
}


void DialogOperation::on_addPushButton_clicked()
{
    Transformation transformation;
    bool transformationResult = false;

    DialogTransformation dialog(0,&transformation, &transformationResult, false);
    dialog.setModal(true);
    dialog.exec();

    if(transformationResult == true)
    {
        node->transformList.push_back(transformation);
        updateTransformationsList();
    }
}

void DialogOperation::on_deletePushButton_clicked()
{
    int index = ui->transformationsListWidget->currentRow();

    if(index >= 0)
    {
        node->transformList.erase(node->transformList.begin()+index);
        updateTransformationsList();
    }
}

void DialogOperation::on_editPushButton_clicked()
{
    int index = ui->transformationsListWidget->currentRow();
    Transformation transformation;
    bool transformationResult = false;

    if(index >= 0)
    {
         transformation = node->transformList[index];

         DialogTransformation dialog(this, &transformation, &transformationResult, true);
         dialog.setModal(true);
         dialog.exec();

         if(transformationResult)
         {
             node->transformList[index] = transformation;
             updateTransformationsList();
         }
    }
}

void DialogOperation::on_upPushButton_clicked()
{
    int index = ui->transformationsListWidget->currentRow();

    if(index >= 1)
    {
        std::swap(node->transformList[index], node->transformList[index-1]);
        updateTransformationsList();
        ui->transformationsListWidget->setCurrentRow(index-1);
    }
}

void DialogOperation::on_downPushButton_clicked()
{
    int index = ui->transformationsListWidget->currentRow();

    if(index < node->transformList.size()-1)
    {
        std::swap(node->transformList[index], node->transformList[index+1]);
        updateTransformationsList();
        ui->transformationsListWidget->setCurrentRow(index+1);
    }
}
