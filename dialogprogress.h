#ifndef DIALOGPROGRESS_H
#define DIALOGPROGRESS_H

#include <QDialog>
#include <QString>
#include <QCloseEvent>
#include <QThread>
#include <QMessageBox>

#include <vector>
#include <fstream>
#include <string>
#include <sstream>

#include "csgtree.h"
#include "union.h"
#include "difference.h"
#include "intersection.h"
#include "box.h"
#include "cylinder.h"
#include "sphere.h"

namespace Ui {
class DialogProgress;
}

class DialogProgress : public QDialog
{
	Q_OBJECT

	QString file;
	CSGTree *tree;
	bool	saving;
	
public:
	explicit DialogProgress(QWidget *parent = 0);
	DialogProgress(QWidget *parent, QString file, CSGTree *tree, bool saving);
	~DialogProgress();
	
private:
	Ui::DialogProgress *ui;

	void closeDialog();

	void save();
	void load();
	void buildTreeRoots();
	void buildTreeChilds(CSGNode *node);

	bool treeIsValid();
	bool treeIsValid(CSGNode *root);

	void writeTransformations(FILE *file, CSGNode *node);

};

#endif // DIALOGPROGRESS_H
