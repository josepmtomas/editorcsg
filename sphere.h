#ifndef SPHERE_H
#define SPHERE_H

#include "csgnode.h"

class Sphere : public CSGNode
{
public:

    vec3    center;
    float   radius;

    Sphere();

    QString getListString();
	QString getDescription();
    void propagateTransformations();

	bool rayIntersection(vec3 origin, vec3 ray, int numPrimitives, RayIntersection &intersection);
	bool rayIntersection(vec3 origin, vec3 ray, RayIntersection *intersections, int2 *arrayIndex);
	bool rayIntersection(vec3 origin, vec3 ray, RayIntersection &in, RayIntersection &out);
};

#endif // SPHERE_H
