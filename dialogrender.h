#ifndef DIALOGRENDER_H
#define DIALOGRENDER_H

#include <QDialog>
#include "csgnode.h"
#include "camera.h"

namespace Ui {
class DialogRender;
}

class DialogRender : public QDialog
{
    Q_OBJECT
    
public:
	CSGNode *node;
	int numPrimitives;
	Camera camera; 

    explicit DialogRender(QWidget *parent = 0);
	DialogRender(QWidget *parent, CSGNode *node, int numPrimitives);
    ~DialogRender();

	void updateRender();
    
private slots:

	void on_horizontalSliderX_valueChanged(int value);

	void on_horizontalSliderZ_valueChanged(int value);

	void on_doubleSpinBox_valueChanged(double arg1);

private:
    Ui::DialogRender *ui;
};

#endif // DIALOGRENDER_H
