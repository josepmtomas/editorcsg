#ifndef RAYINTERSECTION_H
#define RAYINTERSECTION_H

#include "Algebra.h"

class RayIntersection
{
public:
	int		type;
	float	distance;
	vec3	normal;

	RayIntersection();

	bool operator <  (const RayIntersection& i) const {return distance <  i.distance;}
	bool operator <= (const RayIntersection& i) const {return distance <= i.distance;}
	bool operator >  (const RayIntersection& i) const {return distance >  i.distance;}
	bool operator >= (const RayIntersection& i) const {return distance >= i.distance;}
};

#endif // RAYINTERSECTION_H
