#include "dialogcylinder.h"
#include "ui_dialogcylinder.h"

DialogCylinder::DialogCylinder(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogCylinder)
{
    ui->setupUi(this);
}

DialogCylinder::DialogCylinder(QWidget *parent, CSGTree *_tree, Cylinder *_cylinder, bool *_result, bool _editing) :
    QDialog(parent),
    ui(new Ui::DialogCylinder)
{
    cylinder = _cylinder;
    result = _result;
    tree = _tree;
    editing = _editing;

    ui->setupUi(this);

    initialName = cylinder->name;
    ui->nameLineEdit->setText(cylinder->name);
    ui->nameLineEdit->update();

    /*localCylinder.axis     = cylinder->axis;
    localCylinder.base     = cylinder->base;
    localCylinder.center   = cylinder->center;
    localCylinder.height   = cylinder->height;
    localCylinder.name     = cylinder->name;
    localCylinder.parent   = cylinder->parent;
    localCylinder.pivotOnCenter = cylinder->pivotOnCenter;
    localCylinder.radius   = cylinder->radius;
    localCylinder.transformList = cylinder->transformList;
    localCylinder.type     = cylinder->type;*/

    localCylinder = *cylinder;

    updateValues();
	updateTransformationsList();
}

DialogCylinder::~DialogCylinder()
{
    delete ui;
}

void DialogCylinder::updateValues()
{
    ui->baseXSpinBox->setValue(localCylinder.base.x);   ui->baseXSpinBox->update();
    ui->baseYSpinBox->setValue(localCylinder.base.y);   ui->baseYSpinBox->update();
    ui->baseZSpinBox->setValue(localCylinder.base.z);   ui->baseZSpinBox->update();

    ui->centerXSpinBox->setValue(localCylinder.center.x);   ui->centerXSpinBox->update();
    ui->centerYSpinBox->setValue(localCylinder.center.y);   ui->centerYSpinBox->update();
    ui->centerZSpinBox->setValue(localCylinder.center.z);   ui->centerZSpinBox->update();

    ui->axisXSpinBox->setValue(localCylinder.axis.x);    ui->axisXSpinBox->update();
    ui->axisYSpinBox->setValue(localCylinder.axis.y);    ui->axisYSpinBox->update();
    ui->axisZSpinBox->setValue(localCylinder.axis.z);    ui->axisZSpinBox->update();

    ui->radiusSpinBox->setValue(localCylinder.radius);  ui->radiusSpinBox->update();

    ui->heightSpinBox->setValue(localCylinder.height); ui->heightSpinBox->update();

    if(localCylinder.pivotOnCenter)
    {
        ui->pivotCenterRadioButton->setChecked(true);   ui->pivotCenterRadioButton->update();
        ui->pivotBaseRadioButton->setChecked(false);    ui->pivotBaseRadioButton->update();
    }
    else
    {
        ui->pivotCenterRadioButton->setChecked(false);  ui->pivotCenterRadioButton->update();
        ui->pivotBaseRadioButton->setChecked(true);     ui->pivotBaseRadioButton->update();
    }
}

void DialogCylinder::updateTransformationsList()
{
	ui->transformationsListWidget->clear();

	for(int i=0; i < localCylinder.transformList.size(); i++)
	{
		ui->transformationsListWidget->addItem(localCylinder.transformList[i].getListString());
	}
}

void DialogCylinder::accept()
{
    if(*result == true)
    {
        *cylinder = localCylinder;
       // delete localCylinder;
        DialogCylinder::close();
    }
}

void DialogCylinder::on_heightSpinBox_valueChanged(double arg1)
{
	localCylinder.height = (float)arg1;
	localCylinder.heightChanged();
    updateValues();
}

void DialogCylinder::on_radiusSpinBox_valueChanged(double arg1)
{
    localCylinder.radius = (float)arg1;
    localCylinder.radiusChanged();
    updateValues();
}

void DialogCylinder::on_baseXSpinBox_valueChanged(double arg1)
{
    localCylinder.base.x = (float)arg1;
    localCylinder.baseChanged();
    updateValues();
}

void DialogCylinder::on_baseYSpinBox_valueChanged(double arg1)
{
    localCylinder.base.y = (float)arg1;
    localCylinder.baseChanged();
    updateValues();
}

void DialogCylinder::on_baseZSpinBox_valueChanged(double arg1)
{
    localCylinder.base.z = (float)arg1;
    localCylinder.baseChanged();
    updateValues();
}

void DialogCylinder::on_centerXSpinBox_valueChanged(double arg1)
{
    localCylinder.center.x = (float)arg1;
    localCylinder.centerChanged();
    updateValues();
}

void DialogCylinder::on_centerYSpinBox_valueChanged(double arg1)
{
    localCylinder.center.y = (float)arg1;
    localCylinder.centerChanged();
    updateValues();
}

void DialogCylinder::on_centerZSpinBox_valueChanged(double arg1)
{
	localCylinder.center.z = (float)arg1;
    localCylinder.centerChanged();
    updateValues();
}

void DialogCylinder::on_axisXSpinBox_valueChanged(double arg1)
{
    localCylinder.axis.x = (float)arg1;
    localCylinder.axisChanged();
    updateValues();
}

void DialogCylinder::on_axisYSpinBox_valueChanged(double arg1)
{
    localCylinder.axis.y = (float)arg1;
    localCylinder.axisChanged();
    updateValues();
}

void DialogCylinder::on_axisZSpinBox_valueChanged(double arg1)
{
    localCylinder.axis.z = (float)arg1;
    localCylinder.axisChanged();
    updateValues();
}

void DialogCylinder::on_buttonBox_accepted()
{
    if(editing)
    {
		if(ui->nameLineEdit->text().isEmpty())
		{
			QMessageBox::warning(this, "Editar un cilindro", "El cilindro debe de tener un nombre");
		}
		else if(ui->nameLineEdit->text().compare(initialName) != 0)
        {
            if(tree->nameExists(ui->nameLineEdit->text()))
            {
                QMessageBox::warning(this, "Editar un cilindro", "El nombre del objeto ya existe");
            }
            else
            {
                *result = true;
                accept();
            }
        }
        else
        {
            *result = true;
            accept();
        }
    }
    else
    {
        if(ui->nameLineEdit->text().isEmpty())
        {
            QMessageBox::warning(this, "Crear un cilindro", "El cilindro debe de tener un nombre");
        }
        else
        {
            if(tree->nameExists(ui->nameLineEdit->text()))
            {
                QMessageBox::warning(this, "Crear un cilindro", "El nombre del objeto ya existe");
            }
            else
            {
                *result = true;
                accept();
            }
        }
    }
}

void DialogCylinder::on_buttonBox_rejected()
{
    *result = false;
}

void DialogCylinder::on_pivotCenterRadioButton_toggled(bool checked)
{
    localCylinder.pivotOnCenter = checked;
    localCylinder.pivotChanged();
}

void DialogCylinder::on_pivotBaseRadioButton_toggled(bool checked)
{
    localCylinder.pivotOnCenter = !checked;
    localCylinder.pivotChanged();
}

void DialogCylinder::on_nameLineEdit_textChanged(const QString &arg1)
{
    localCylinder.name = arg1;
}

void DialogCylinder::on_addPushButton_clicked()
{
	Transformation transformation;
	bool transformationResult = false;

	DialogTransformation dialog(0, &transformation, &transformationResult, false);
	dialog.setModal(true);
	dialog.exec();

	if(transformationResult == true)
	{
		localCylinder.transformList.push_back(transformation);
		updateTransformationsList();
	}
}

void DialogCylinder::on_deletePushButton_clicked()
{
	int index = ui->transformationsListWidget->currentRow();

	if(index >= 0)
	{
		localCylinder.transformList.erase(localCylinder.transformList.begin()+index);
		updateTransformationsList();
	}
}

void DialogCylinder::on_editPushButton_clicked()
{
	int index = ui->transformationsListWidget->currentRow();
	Transformation transformation;
	bool transformationResult = false;

	if(index >= 0)
	{
		transformation = localCylinder.transformList[index];

		DialogTransformation dialog(0, &transformation, &transformationResult, true);
		dialog.setModal(true);
		dialog.exec();

		if(transformationResult == true)
		{
			localCylinder.transformList[index] = transformation;
			updateTransformationsList();
		}
	}
}

void DialogCylinder::on_upPushButton_clicked()
{
	int index = ui->transformationsListWidget->currentRow();

	if(index >= 1)
	{
		std::swap(localCylinder.transformList[index], localCylinder.transformList[index-1]);
		updateTransformationsList();
		ui->transformationsListWidget->setCurrentRow(index-1);
	}
}

void DialogCylinder::on_downPushButton_clicked()
{
	int index = ui->transformationsListWidget->currentRow();

	if(index < localCylinder.transformList.size()-1)
	{
		std::swap(localCylinder.transformList[index], localCylinder.transformList[index+1]);
		updateTransformationsList();
		ui->transformationsListWidget->setCurrentRow(index+1);
	}
}


