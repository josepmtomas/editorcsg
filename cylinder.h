#ifndef CYLINDER_H
#define CYLINDER_H

#include "Algebra.h"
#include "csgnode.h"
#include <QString>

class Cylinder : public CSGNode
{
public:

	typedef enum CylinderSurface { SIDE, BOTTOM, TOP };

    vec3    base;
    vec3    center;
    vec3    axis;
	vec4	bottom;
	vec4	top;
    float   radius;
    float   height;

    bool    pivotOnCenter;

    Cylinder();

    void baseChanged();
    void centerChanged();
    void axisChanged();
    void radiusChanged();
    void heightChanged();
    void pivotChanged();

	void buildPlanes();

    QString getListString();
	QString getDescription();
    void propagateTransformations();

	bool rayIntersection(vec3 origin, vec3 ray, int numPrimitives, RayIntersection &intersection);
	bool rayIntersection(vec3 origin, vec3 ray, RayIntersection *intersections, int2 *arrayIndex);
	bool rayIntersection(vec3 origin, vec3 ray, RayIntersection &in, RayIntersection &out);
};

#endif // CYLINDER_H
