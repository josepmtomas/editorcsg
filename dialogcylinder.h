#ifndef DIALOGCYLINDER_H
#define DIALOGCYLINDER_H

#include <QDialog>
#include <QMessageBox>
#include <QCloseEvent>
#include "csgtree.h"
#include "cylinder.h"
#include "dialogtransformation.h"

namespace Ui {
class DialogCylinder;
}

class DialogCylinder : public QDialog
{
    Q_OBJECT
    
public:

    CSGTree     *tree;
    Cylinder    *cylinder;
    Cylinder    localCylinder;
    bool        *result;
    bool        editing;
    QString     initialName;

    explicit DialogCylinder(QWidget *parent = 0);
    explicit DialogCylinder(QWidget *parent, CSGTree *tree, Cylinder *_cylinder, bool *_result, bool _editing);
    ~DialogCylinder();

    void updateValues();
    void updateTransformationsList();

    void accept();
    
private slots:
    void on_heightSpinBox_valueChanged(double arg1);
    void on_radiusSpinBox_valueChanged(double arg1);

    void on_baseXSpinBox_valueChanged(double arg1);
    void on_baseYSpinBox_valueChanged(double arg1);
    void on_baseZSpinBox_valueChanged(double arg1);

    void on_centerXSpinBox_valueChanged(double arg1);
    void on_centerYSpinBox_valueChanged(double arg1);
    void on_centerZSpinBox_valueChanged(double arg1);

    void on_axisXSpinBox_valueChanged(double arg1);
    void on_axisYSpinBox_valueChanged(double arg1);
    void on_axisZSpinBox_valueChanged(double arg1);

    void on_buttonBox_accepted();
    void on_buttonBox_rejected();

    void on_pivotCenterRadioButton_toggled(bool checked);
    void on_pivotBaseRadioButton_toggled(bool checked);

    void on_nameLineEdit_textChanged(const QString &arg1);

    void on_addPushButton_clicked();
    void on_deletePushButton_clicked();
	void on_editPushButton_clicked();
	void on_upPushButton_clicked();
	void on_downPushButton_clicked();



private:
    Ui::DialogCylinder *ui;
};

#endif // DIALOGCYLINDER_H
