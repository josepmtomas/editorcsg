#include "glwidget.h"

GLWidget::GLWidget(QWidget *parent) :
	QGLWidget(parent)
{
	this->setFocusPolicy(Qt::ClickFocus);

	raster = NULL;
}

GLWidget::~GLWidget()
{
	if(raster) delete raster;

	for(int i=0; i<width()*height(); i++)
	{
		delete intersections[i];
	}
	intersections.clear();
}

void GLWidget::update()
{
	updateGL();
}

void GLWidget::rayTrace(CSGNode *node, int numPrimitives, Camera *camera)
{
	/*#pragma omp parallel for
	for(int i=0; i<height(); i++)
		for(int j=0; j<width(); j++)
		{
			int position = (i*width()*3) + (j*3);
			vec3 color;
			RayIntersection intersection = RayIntersection();

			if(node->rayIntersection(camera->pov,camera->visualPixel(j,i,width(),height()),numPrimitives,intersection))
			{
				color = vec3(0.1,0.1,1.0) * dot(normalize(intersection.normal),
												normalize(camera->pov-camera->poi));
			}
			else
			{
				color = vec3(0,0,0);
			}

			raster[position]	= (unsigned char) (color.x*255);
			raster[position+1]	= (unsigned char) (color.y*255);
			raster[position+2]	= (unsigned char) (color.z*255);
		}*/

	for(int i=0; i<height(); i++)
		for(int j=0; j<width(); j++)
		{
			int position = (i*width()*3) + (j*3);
			vec3 color;
			int2 index = int2(0,0);

			if(node->rayIntersection(camera->pov,
									 camera->visualPixel(j,i,width(),height()),
									 intersections[i*width()+j],
									 &index))
			{
				color = vec3(0.2,0.2,1.0) * dot(normalize(intersections[i*width()+j][0].normal),
												normalize(camera->pov-camera->poi));

				color.x < 0.0 ? color.x = 0.0 : 0 ;
				color.y < 0.0 ? color.y = 0.0 : 0 ;
				color.z < 0.0 ? color.z = 0.0 : 0 ;

				color = color + (vec3(0.5,0.5,0.5) * powf(dot(normalize(intersections[i*width()+j][0].normal),normalize(camera->pov-camera->poi)),2));
				color.x > 1.0 ? color.x = 1.0 : 0 ;
				color.y > 1.0 ? color.y = 1.0 : 0 ;
				color.z > 1.0 ? color.z = 1.0 : 0 ;
			}
			else
			{
				color = vec3(0.0f);
			}

			raster[position]	= (unsigned char) (color.x*255);
			raster[position+1]	= (unsigned char) (color.y*255);
			raster[position+2]	= (unsigned char) (color.z*255);
		}

	updateGL();

	//RayIntersection intersection = RayIntersection();
	//node->rayIntersection(camera->pov,camera->visualPixel(150,150,width(),height()),numPrimitives,intersection);
}

void GLWidget::initializeIntersections(int numPrimitives)
{
	int size = height()*width();
	RayIntersection *intersection;

	for(int i=0; i<size; i++)
	{
		intersection = new RayIntersection[numPrimitives*2];

		intersections.push_back(intersection);
	}
}

void GLWidget::initializeGL()
{
	glClearColor(0.25f, 0.25f, 0.25f, 1.0f);

	if(raster) delete raster;

	raster = new unsigned char[width()*height()*3];

	for(int i=0; i<width()*height()*3; i++)
	{
		raster[i] = 0;
	}

	/*int position;

	for(int i=0; i<height(); i++)
		for(int j=0; j<width(); j++)
		{
			position = (i*width()*3) + (j*3);

			raster[position]	= (unsigned char)(((float)i/(float)height())*255);
			raster[position+1]	= (unsigned char)(((float)j/(float)width())*255);
			raster[position+2]	= 0;
		}*/

}

void GLWidget::paintGL()
{
	glClear(GL_COLOR_BUFFER_BIT);

	glViewport(-width()/2, -height()/2, width(), height());
	glRasterPos2i(0,0);
	glDrawPixels(width(),height(),GL_RGB,GL_UNSIGNED_BYTE, raster);

	glFlush();
}

void GLWidget::resizeGL(int w, int h)
{

}
