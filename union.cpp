#include "union.h"

Union::Union()
{
	this->type = NodeType::OP_UNION;
	this->parent = -1;
	this->leftChild = NULL;
	this->rightChild = NULL;
	this->leftIndex = -1;
	this->rightIndex = -1;
	this->leftId = -1;
	this->rightId = -1;
}

QString Union::getListString()
{
	QString nameString;

	nameString = QString("[UNI] ");
    nameString.append(this->name);
	nameString.append(": ");

	if(leftChild)
		nameString.append(leftChild->name);
	else
		nameString.append("NONE");

	nameString.append(" - ");

	if(rightChild)
		nameString.append(rightChild->name);
	else
		nameString.append("NONE");

	return nameString;
}

QString Union::getDescription()
{
	return QString("");
}

void Union::propagateTransformations()
{
    for(int i=0; i<transformList.size(); i++)
    {
		leftChild->transformList.push_back(this->transformList.at(i));
		rightChild->transformList.push_back(this->transformList.at(i));
    }
	leftChild->propagateTransformations();
	rightChild->propagateTransformations();
}

bool Union::rayIntersection(vec3 origin, vec3 ray, int numPrimitives, RayIntersection &intersection)
{
	RayIntersection in = RayIntersection();
	RayIntersection out = RayIntersection();

	RayIntersection *intersections = new RayIntersection[numPrimitives*2];
	int2 arrayIndex = int2(0,0);

	/*if(leftChild->rayIntersection(origin,ray,in,out))
	{
		qDebug("TRUE");
		intersection = in;
		return true;
	}
	return false;*/

	if(rayIntersection(origin,ray,intersections,&arrayIndex))
	{
		intersection = intersections[0];
		delete intersections;
		//qDebug("TRUE");
		return true;
	}
	else
	{
		delete intersections;
		return false;
	}
}

bool Union::rayIntersection(vec3 origin, vec3 ray, RayIntersection *intersections, int2 *arrayIndex)
{
	RayIntersection x;
	int2 indexA;
	int2 indexB;
	bool hitA, hitB;
	int i,j,k;
	int current,previous,state;

	indexA.x = arrayIndex->y;
	indexA.y = arrayIndex->y;

	// Test intersections with LEFT CHILD

	if( leftChild->type == NodeType::OP_UNION ||
		leftChild->type == NodeType::OP_DIFFERENCE ||
		leftChild->type == NodeType::OP_INTERSECTION)
	{
		leftChild->rayIntersection(origin,ray,intersections,&indexA);
	}
	else
	{
		if(leftChild->rayIntersection(origin,ray,intersections[indexA.y],intersections[indexA.y+1]))
			indexA.y += 1;
	}

	if(indexA.x >= indexA.y)
	{
		hitA = false;
		indexB = int2(arrayIndex->y, arrayIndex->y);
	}
	else
	{
		hitA = true;
		indexB = int2(indexA.y+1, indexA.y+1);
	}

	// Test intersections with RIGHT CHILD

	if( rightChild->type == NodeType::OP_UNION ||
		rightChild->type == NodeType::OP_DIFFERENCE ||
		rightChild->type == NodeType::OP_INTERSECTION)
	{
		rightChild->rayIntersection(origin,ray,intersections,&indexB);
	}
	else
	{
		if(rightChild->rayIntersection(origin,ray,intersections[indexB.y],intersections[indexB.y+1]))
			indexB.y += 1;
	}

	if(indexB.x >= indexB.y)
	{
		hitB = false;
	}
	else
	{
		hitB = true;
	}

	//////////////////////////////////////////////////////////////

	if(hitA && !hitB)
	{
		arrayIndex->y = indexA.y;
	}
	else if(!hitA && hitB)
	{
		arrayIndex->y = indexB.y;
	}
	else if(!hitA && !hitB)
	{
		return false;
	}

	// If hitA and hitB -> Sort the subarray [A,B]
	int incr = (indexB.y-indexA.x+1)/2;

	// Shell sort
	while(incr > 0)
	{
		for(i=incr+indexA.x; i<=indexB.y; i++)
		{
			x = intersections[i];
			j = i;
			while((j >= incr+indexA.x) && (x.distance < intersections[j-incr].distance))
			{
				intersections[j] = intersections[j-incr];
				j -= incr;
			}
			intersections[j] = x;
		}
		incr = incr/2;
	}

	i = indexA.x+1;

	while(i <= indexB.y)
	{
		previous = intersections[i-1].type;
		current = intersections[i].type;

		if(current == INTERSECTION_IN && previous == INTERSECTION_IN)
		{
			k = i+1;
			while(k <= indexB.y)
			{
				intersections[k-1] = intersections[k];
				k++;
			}
			indexB.y = indexB.y-1;
		}
		else if(previous == INTERSECTION_OUT && current == INTERSECTION_OUT)
		{
			k = i;
			while(k <= indexB.y)
			{
				intersections[k-1] = intersections[k];
				k++;
			}
			indexB.y = indexB.y - 1;
		}
		else
		{
			i++;
		}
	}

	if(indexA.x < indexB.y)				// If there is at least an pair of intersections (IN/OUT)
	{
		//arrayIndex->x = indexA.x;
		arrayIndex->y = indexB.y;		// The index of the last element of the recieved array has increased
		return true;
	}
	else
	{
		return false;
	}

}

bool Union::rayIntersection(vec3 origin, vec3 ray, RayIntersection &in, RayIntersection &out)
{
	return false;
}

