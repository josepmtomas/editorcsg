#include "sphere.h"

Sphere::Sphere()
{
    type = NodeType::SPHERE;
	leftChild = NULL;
	rightChild = NULL;
	leftIndex = -1;
	rightIndex = -1;
	leftId = -1;
	rightId = -1;
    center = vec3(0,0,0);
    radius = 1.0;
}

QString Sphere::getListString()
{
    QString string;
    QString numString;

	string.append("[SPH] ");
    string.append(name);
    string.append(": ");

    string.append("Radio(");
    numString.setNum(radius,'g',4);
    string.append(numString);
    string.append(") ");

    string.append("Centro(");
    numString.setNum(center.x,'g',4);
    string.append(numString);
    string.append(",");
    numString.setNum(center.y,'g',4);
    string.append(numString);
    string.append(",");
    numString.setNum(center.z,'g',4);
    string.append(numString);
    string.append(") ");

    return string;
}

QString Sphere::getDescription()
{
	QString string;
	QString numString;

	string.append("Radio(");
	numString.setNum(radius,'g',4);
	string.append(numString);
	string.append(") ");

	string.append("Centro(");
	numString.setNum(center.x,'g',4);
	string.append(numString);
	string.append(",");
	numString.setNum(center.y,'g',4);
	string.append(numString);
	string.append(",");
	numString.setNum(center.z,'g',4);
	string.append(numString);
	string.append(") ");

	return string;
}

void Sphere::propagateTransformations()
{
	Transformation t;

	qDebug("NUM TRANSFORMATIONS IN SPHERE= %d", transformList.size());

	for(int i=0; i<transformList.size(); i++)
	{
		t = transformList[i];

		if(t.type == TransformationType::TRANSLATION)
		{
			//qDebug("TRANSLATE X = %.2f Y = %.2f Z = %.2f", t.vectorValue.x, t.vectorValue.y, t.vectorValue.z);
			center = center + t.vectorValue;
			qDebug("CENTER = %.2f %.2f %.2f", center.x, center.y, center.z);
		}
		else if(t.type == TransformationType::SCALE)
		{
			//qDebug("CENTER = %.2f %.2f %.2f", center.x, center.y, center.z);
			//qDebug("RADIUS %.2f", radius);
			center = center * t.vectorValue;
			radius = radius * t.vectorValue.x;
			//qDebug("CENTER = %.2f %.2f %.2f", center.x, center.y, center.z);
			//qDebug("RADIUS %.2f", radius);
		}
	}
}


bool Sphere::rayIntersection(vec3 origin, vec3 ray, int numPrimitives, RayIntersection &intersection)
{
	return false;
}

bool Sphere::rayIntersection(vec3 origin, vec3 ray, RayIntersection *intersections, int2 *arrayIndex)
{
	return false;
}

bool Sphere::rayIntersection(vec3 origin, vec3 ray, RayIntersection &in, RayIntersection &out)
{
	float alpha, beta, gamma;
	float root;
	float t1,t2;
	vec3 pcenter = origin - center;

	alpha = dot(ray,ray);
	beta = 2 * dot(pcenter,ray);
	gamma = dot(pcenter,pcenter) - (radius*radius);

	root = beta*beta - 4*alpha*gamma;

	if(root < 0.0f)
	{
		in.type = INTERSECTION_NONE;
		out.type = INTERSECTION_NONE;
		return false;
	}

	root = sqrtf(root);
	t2 = (-beta+root)/(2*alpha);
	t1 = (-beta-root)/(2*alpha);

	if(t1<=0)
	{
		in.type = INTERSECTION_NONE;
		out.type = INTERSECTION_NONE;
		return false;
	}

	in.type				= INTERSECTION_IN;
	//in.objectType		= type;
	//in.objectIndex		= index;
	in.distance			= t1;
	//in.point			= origin + ray*t1;
	in.normal			= normalize((origin + ray*t1) - center);

	out.type			= INTERSECTION_OUT;
	//out.objectType		= type;
	//out.objectIndex		= index;
	out.distance		= t2;
	//out.point			= origin + ray*t2;
	out.normal			= normalize((origin + ray*t2) - center);

	return true;
}
