#include "cylinder.h"

Cylinder::Cylinder()
{
    type = NodeType::CYLINDER;
	leftChild = NULL;
	rightChild = NULL;
	leftIndex = -1;
	rightIndex = -1;
	leftId = -1;
	rightId = -1;

    base = vec3(0,0,0);
    center = vec3(0,0.5,0);
    axis = vec3(0,1,0);
    radius = 1.0;
    height = 1.0;
    pivotOnCenter = false;
}

void Cylinder::baseChanged()
{
    center = base + axis*(height/2);
	buildPlanes();
}

void Cylinder::centerChanged()
{
    base = center - axis*(height/2.0f);
	buildPlanes();
}

void Cylinder::axisChanged()
{
	if(axis == vec3(0,0,0))
		axis = vec3(0,1,0);

    axis = normalize(axis);

    if(pivotOnCenter)
    {
        base = center - axis*(height/2.0f);
    }
    else
    {
        center = base + axis*(height/2.0f);
    }
	buildPlanes();
}

void Cylinder::radiusChanged()
{
	buildPlanes();
}

void Cylinder::heightChanged()
{
    if(pivotOnCenter)
    {
        base = center - axis*(height/2.0f);
    }
    else
    {
        center = base + axis*(height/2.0f);
    }
	buildPlanes();
}

void Cylinder::pivotChanged()
{

}

void Cylinder::buildPlanes()
{
	// Bottom plane

	vec3 point = base;
	vec3 normal = negate(axis);
	float d;

	d = normal.x*point.x + normal.y*point.y + normal.z*point.z;
	bottom = vec4(normal.x, normal.y, normal.z, -d);

	// Top plane

	point = base + axis*height;
	normal = axis;

	d = normal.x*point.x + normal.y*point.y + normal.z*point.z;
	top = vec4(normal.x, normal.y, normal.z, -d);
}

QString Cylinder::getListString()
{
    QString string;
    QString numString;

	string.append("[CYL] ");
    string.append(name);
    string.append(": ");

    string.append("Radio(");
    numString.setNum(radius,'g',4);
    string.append(numString);
    string.append(") ");

    string.append("Altura(");
    numString.setNum(height,'g',4);
    string.append(numString);
    string.append(") ");

    string.append("Base(");
    numString.setNum(base.x,'g',4);
    string.append(numString);
    string.append(",");
    numString.setNum(base.y,'g',4);
    string.append(numString);
    string.append(",");
    numString.setNum(base.z,'g',4);
    string.append(numString);
    string.append(") ");

    string.append("Centro(");
    numString.setNum(center.x,'g',4);
    string.append(numString);
    string.append(",");
    numString.setNum(center.y,'g',4);
    string.append(numString);
    string.append(",");
    numString.setNum(center.z,'g',4);
    string.append(numString);
    string.append(") ");

    string.append("Eje(");
    numString.setNum(axis.x,'g',4);
    string.append(numString);
    string.append(",");
    numString.setNum(axis.y,'g',4);
    string.append(numString);
    string.append(",");
    numString.setNum(axis.z,'g',4);
    string.append(numString);
    string.append(") ");



    return string;
}

QString Cylinder::getDescription()
{
	QString string;
	QString numString;

	string.append("Radio(");
	numString.setNum(radius,'g',4);
	string.append(numString);
	string.append(") ");

	string.append("Altura(");
	numString.setNum(height,'g',4);
	string.append(numString);
	string.append(") ");

	string.append("Base(");
	numString.setNum(base.x,'g',4);
	string.append(numString);
	string.append(",");
	numString.setNum(base.y,'g',4);
	string.append(numString);
	string.append(",");
	numString.setNum(base.z,'g',4);
	string.append(numString);
	string.append(") ");

	string.append("Centro(");
	numString.setNum(center.x,'g',4);
	string.append(numString);
	string.append(",");
	numString.setNum(center.y,'g',4);
	string.append(numString);
	string.append(",");
	numString.setNum(center.z,'g',4);
	string.append(numString);
	string.append(") ");

	string.append("Eje(");
	numString.setNum(axis.x,'g',4);
	string.append(numString);
	string.append(",");
	numString.setNum(axis.y,'g',4);
	string.append(numString);
	string.append(",");
	numString.setNum(axis.z,'g',4);
	string.append(numString);
	string.append(") ");

	return string;
}

void Cylinder::propagateTransformations()
{
    for(int i=0; i<transformList.size(); i++)
    {
        switch(transformList[i].type)
        {
        case TransformationType::TRANSLATION:
            if(pivotOnCenter)
            {
                center = center + transformList[i].vectorValue;
                centerChanged();
            }
            else
            {
                base = base + transformList[i].vectorValue;
				baseChanged();
            }
            break;

        case TransformationType::ROTATION:
            axis = rotate(axis,transformList[i].vectorValue, transformList[i].floatValue);
            axisChanged();
            break;

        case TransformationType::SCALE:
            radius = radius * transformList[i].vectorValue.x;
            radiusChanged();

            height = height * transformList[i].vectorValue.y;
            heightChanged();
            break;
        default:
            break;
        }
	}

	buildPlanes();
}

bool Cylinder::rayIntersection(vec3 origin, vec3 ray, int numPrimitives, RayIntersection &intersection)
{
	return false;
}

bool Cylinder::rayIntersection(vec3 origin, vec3 ray, RayIntersection *intersections, int2 *arrayIndex)
{
	return false;
}

bool Cylinder::rayIntersection(vec3 origin, vec3 ray, RayIntersection &in, RayIntersection &out)
{
	vec3	RC;				// Ray base to cylinder base
	float	d;				// Shortest distance between the ray and the cylinder
	float	t,s;			// Distances along the ray
	vec3	n,D,O;
	float	ln;
	float	din;			// Entering distance
	float	dout;			// Leaving distance
	bool	hit = false;
	int		surfin;			// Entering surface identifier
	int		surfout;		// Exiting surface identifier

	float	dc,dw;
	//float	in,out;			// Object intersection distances

	RC = origin - base;
	n = cross(ray,axis);
	ln = module(n);

	if(ln == 0.0f)								// ray parallel to cylinder
	{
		d	= dot(RC,axis);
		D	= RC - d*axis;
		d	= module(D);
		din	= -INFINITE;
		dout = INFINITE;
		hit = (d <= radius);					// TRUE if ray is in cyl
	}
	else
	{
		n = normalize(n);
		d = fabs(dot(RC,n));					// Shortest distance
		hit = (d <= radius);

		if(hit)									// If ray hits cylinder
		{
			O	= cross(RC,axis);
			t	= - dot(O,n) / ln;
			O	= cross(n,axis);
			O	= normalize(O);
			s	= fabs(sqrt(radius*radius - d*d)) / dot(ray,O);
			din	= t + s;
			dout = t - s;
		}
	}

	if(!hit)
	{
		return false;
	}
	else
	{
		surfin = surfout = CylinderSurface::SIDE;

		// Intersect the ray with the bottom end-cap plane

		dc = bottom.x*ray.x + bottom.y*ray.y + bottom.z*ray.z;
		dw = bottom.x*origin.x + bottom.y*origin.y + bottom.z*origin.z + bottom.w;

		if(dc == 0)			// If parallel to bottom plane
		{
			if(dw >= 0.0f) return false;
		}
		else
		{
			t = -dw/dc;
			if(dc >= 0.0f)	// If far plane
			{
				if( t > din && t < dout )
				{
					dout = t;
					surfout = CylinderSurface::BOTTOM;
				}
				if(t < din) return false;
			}
			else			// If near plane
			{
				if( t > din && t < dout )
				{
					din = t;
					surfin = CylinderSurface::BOTTOM;
				}
				if(t > dout) return false;
			}
		}

		// Intersect the ray with the top end-cap plane

		dc = top.x*ray.x + top.y*ray.y + top.z*ray.z;
		dw = top.x*origin.x + top.y*origin.y + top.z*origin.z + top.w;

		if(dc == 0)			// If parallel to top plane
		{
			if(dw >= 0.0f) return false;
		}
		else
		{
			t = -dw/dc;
			if(dc >= 0.0f)	// If far plane
			{
				if( t > din && t < dout )
				{
					dout = t;
					surfout = CylinderSurface::TOP;
				}
				if(t < din) return false;
			}
			else			// If near plane
			{
				if( t > din && t < dout )
				{
					din = t;
					surfin = CylinderSurface::TOP;
				}
				if(t > dout) return false;
			}
		}

		if(din < dout)
		{
			vec3 C,D;
			float u;

			in.type = INTERSECTION_IN;
			in.distance	= din;
			//in.point	= origin + ray*din;
			out.type = INTERSECTION_OUT;
			out.distance= dout;
			//out.point	= origin + ray*dout;

			// In surface
			switch(surfin)
			{
			case CylinderSurface::BOTTOM:
				in.normal = negate(axis); break;

			case CylinderSurface::TOP:
				in.normal = axis; break;

			case CylinderSurface::SIDE:
				C =(origin + ray*din) - base;
				u = dot(axis,C);
				D = base + axis*u;

				in.normal = normalize((origin + ray*din)-D);
				break;
			default:
				break;
			}

			// Out surface
			switch(surfout)
			{
			case CylinderSurface::BOTTOM:
				out.normal = negate(axis); break;

			case CylinderSurface::TOP:
				out.normal = axis; break;

			case CylinderSurface::SIDE:
				C = (origin + ray*dout) - base;
				u = dot(axis,C);
				D = base + axis*u;

				out.normal = normalize((origin + ray*dout)-D);
				break;
			default:
				break;
			}

			return true;
		}
		else
			return false;
	}
}
