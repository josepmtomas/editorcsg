#ifndef DIALOGBOX_H
#define DIALOGBOX_H

#include <QDialog>
#include "csgtree.h"
#include "box.h"
#include "dialogtransformation.h"

namespace Ui {
class DialogBox;
}

class DialogBox : public QDialog
{
    Q_OBJECT
    
public:

    CSGTree *tree;
    Box     *box;
	vec3    size;
	QString initialName;
    bool    *result;
    bool    editing;
	bool	edited;


    explicit DialogBox(QWidget *parent = 0);
    DialogBox(QWidget *parent, CSGTree *tree, Box *_box, bool *_result, bool editing);
    ~DialogBox();

    void updateTransformationsList();
    void accept();
    
private slots:
    void on_heightSpinBox_valueChanged(double arg1);
    void on_widthSpinBox_valueChanged(double arg1);
    void on_depthSpinBox_valueChanged(double arg1);

    void on_buttonBox_accepted();

    void on_nameLineEdit_textChanged(const QString &arg1);

    void on_addButton_clicked();
    void on_deletePushButton_clicked();
    void on_editPushButton_clicked();
    void on_upPushButton_clicked();
    void on_downPushButton_clicked();

private:
    Ui::DialogBox *ui;
};

#endif // DIALOGBOX_H
