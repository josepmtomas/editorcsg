#include "dialogsphere.h"
#include "ui_dialogsphere.h"

DialogSphere::DialogSphere(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogSphere)
{
    ui->setupUi(this);
}

DialogSphere::DialogSphere(QWidget *parent, CSGTree *_tree, Sphere *_sphere, bool *_result, bool _editing):
    QDialog(parent),
    ui(new Ui::DialogSphere)
{
    tree = _tree;
    sphere = _sphere;
    result = _result;
	editing = _editing;

	localSphere = *_sphere;
	initialName = _sphere->name;

    ui->setupUi(this);

    ui->nameLineEdit->setText(sphere->name);
    ui->nameLineEdit->update();

    updateValues();
	updateTransformationsList();
}

DialogSphere::~DialogSphere()
{
    delete ui;
}

void DialogSphere::updateValues()
{
	ui->centerXSpinBox->setValue(localSphere.center.x);
	ui->centerYSpinBox->setValue(localSphere.center.y);
	ui->centerZSpinBox->setValue(localSphere.center.z);

	ui->radiusSpinBox->setValue(localSphere.radius);
}

void DialogSphere::updateTransformationsList()
{
	ui->transformationsListWidget->clear();

	for(int i=0; i<localSphere.transformList.size(); i++)
	{
		ui->transformationsListWidget->addItem(localSphere.transformList[i].getListString());
	}
}


void DialogSphere::accept()
{
	if(*result == true)
	{
		*sphere = localSphere;
        DialogSphere::close();
	}
}

void DialogSphere::on_buttonBox_accepted()
{
	if(editing)
	{
		if(localSphere.radius == 0.0)
		{
			QMessageBox::warning(this, "Editar una esfera", "La esfera debe tener un radio > 0");
		}
		else if(localSphere.name.isEmpty())
		{
			QMessageBox::warning(this, "Editar una esfera", "La esfera debe tener un nombre");
		}
		else if(localSphere.name.compare(initialName) != 0)
		{
			if(tree->nameExists(localSphere.name))
			{
				QMessageBox::warning(this, "Editar una esfera", "El nombre ya existe");
			}
			else
			{
				*result = true;
				accept();
			}
		}
		else
		{
			*result = true;
			accept();
		}
	}
	else
	{
		if(localSphere.radius == 0.0)
		{
			QMessageBox::warning(this, "Crear una esfera", "La esfera debe tener un radio > 0");
		}
		else if(localSphere.name.isEmpty())
		{
			QMessageBox::warning(this, "Crear una esfera", "La esfera debe tener un nombre");
		}
		else  if(tree->nameExists(localSphere.name))
		{
			QMessageBox::warning(this, "Crear una esfera", "El nombre ya existe");
		}
		else
		{
			*result = true;
			accept();
		}
	}

}

void DialogSphere::on_buttonBox_rejected()
{
    *result = false;
}

void DialogSphere::on_centerXSpinBox_valueChanged(double arg1)
{
	localSphere.center.x = (float)arg1;
}

void DialogSphere::on_centerYSpinBox_valueChanged(double arg1)
{
	localSphere.center.y = (float)arg1;
}

void DialogSphere::on_centerZSpinBox_valueChanged(double arg1)
{
	localSphere.center.z = (float)arg1;
}

void DialogSphere::on_radiusSpinBox_valueChanged(double arg1)
{
	localSphere.radius = (float)arg1;
}

void DialogSphere::on_nameLineEdit_textChanged(QString arg1)
{
	localSphere.name = arg1;
}

void DialogSphere::on_addPushButton_clicked()
{
	Transformation transformation;
	bool transformationResult = false;

	DialogTransformation dialog(0, &transformation, &transformationResult, false);
	dialog.setModal(true);
	dialog.exec();

	if(transformationResult == true)
	{
		localSphere.transformList.push_back(transformation);
		updateTransformationsList();
	}
}

void DialogSphere::on_deletePushButton_clicked()
{
	int index = ui->transformationsListWidget->currentRow();

	if(index >= 0)
	{
		localSphere.transformList.erase(localSphere.transformList.begin()+index);
		updateTransformationsList();
	}
}

void DialogSphere::on_editPushButton_clicked()
{
	int index = ui->transformationsListWidget->currentRow();
	Transformation transformation;
	bool transformationResult = false;

	if(index >= 0)
	{
		transformation = localSphere.transformList[index];

		DialogTransformation dialog(0, &transformation, &transformationResult, true);
		dialog.setModal(true);
		dialog.exec();

		if(transformationResult)
		{
			localSphere.transformList[index] = transformation;
			updateTransformationsList();
		}
	}
}

void DialogSphere::on_upPushButton_clicked()
{
	int index = ui->transformationsListWidget->currentRow();

	if(index >= 1)
	{
		std::swap(localSphere.transformList[index], localSphere.transformList[index-1]);
		updateTransformationsList();
		ui->transformationsListWidget->setCurrentRow(index-1);
	}
}

void DialogSphere::on_downPushButton_clicked()
{
	int index = ui->transformationsListWidget->currentRow();

	if(index < localSphere.transformList.size()-1)
	{
		std::swap(localSphere.transformList[index], localSphere.transformList[index+1]);
		updateTransformationsList();
		ui->transformationsListWidget->setCurrentRow(index+1);
	}
}
