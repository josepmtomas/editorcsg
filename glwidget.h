#ifndef GLWIDGET_H
#define GLWIDGET_H

#include <QWidget>
#include <QTimer>
#include <QGLWidget>
#include <qopengl.h>

#include "csgnode.h"
#include "camera.h"

class GLWidget : public QGLWidget
{
    Q_OBJECT
public:

    explicit GLWidget(QWidget *parent = 0);
	~GLWidget();

	void initializeIntersections(int numPrimitives);
	void update();
	void rayTrace(CSGNode *node, int numPrimitives, Camera *camera);
    
signals:
    
public slots:

protected:

	unsigned char *raster;

	std::vector<RayIntersection*> intersections;



	void initializeGL();
	void paintGL();
	void resizeGL(int w, int h);
    
};

#endif // GLWIDGET_H
