#ifndef BOX_H
#define BOX_H

#include "Algebra.h"
#include "csgnode.h"

class Box : public CSGNode
{	
public:

    vec3    vertices[8];
    vec3    fnormals[6];
    vec3    tangents[6];
    u_int4   faces[6];

    float   width;
    float   height;
    float   depth;

    Box();

    void    build(vec3 size);
    void    rebuild(vec3 size);
	void	recalculateNormalsAndTangents();

    QString getListString();
	QString getDescription();
    void    propagateTransformations();

	void translation(vec3 amount);
	void rotation(vec3 rotationAxis, float rads);
	void scale(vec3 factor);

	bool rayIntersection(vec3 origin, vec3 ray, int numPrimitives,  RayIntersection &intersection);
	bool rayIntersection(vec3 origin, vec3 ray, RayIntersection *intersections, int2 *arrayIndex);
	bool rayIntersection(vec3 origin, vec3 ray, RayIntersection &in, RayIntersection &out);
};

#endif // BOX_H
