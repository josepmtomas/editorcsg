#include "dialogrender.h"
#include "ui_dialogrender.h"

DialogRender::DialogRender(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogRender)
{
    ui->setupUi(this);
}

DialogRender::DialogRender(QWidget *parent, CSGNode *node, int numPrimitives) :
	QDialog(parent),
	ui(new Ui::DialogRender)
{
	qDebug("RECEIVED ROOT ID = %d", node->id);
	qDebug("RECEIVED NUM PRIMITIVES = %d", numPrimitives);

	this->node = node;
	this->numPrimitives = numPrimitives;

	camera = Camera();
	camera.setPointOfView(vec3(3,3,3));

	ui->setupUi(this);

	this->setWindowTitle("Render");

	ui->widget->initializeIntersections(numPrimitives);
}

DialogRender::~DialogRender()
{
    delete ui;
}

void DialogRender::updateRender()
{
	float camDegreeZ = (float)ui->horizontalSliderZ->value();
	float camDegreeX = (float)ui->horizontalSliderX->value();

	camDegreeZ = DEG2RAD(camDegreeZ);
	camDegreeX = DEG2RAD(camDegreeX);

	float a,b,c,d;
	float radius = ui->doubleSpinBox->value();
	vec3 pov;

	c = -1.0 * sinf(camDegreeZ);
	d =  1.0 * cosf(camDegreeZ);
	pov.y = (-c*radius);
	pov.z = d*radius;

	a = -d * sinf(camDegreeX);
	b =  d * cosf(camDegreeX);
	pov.x = a*radius;
	pov.z = b*radius;

	camera.setPointOfView(pov);

	ui->widget->rayTrace(node,numPrimitives,&camera);
}

void DialogRender::on_horizontalSliderX_valueChanged(int value)
{
	updateRender();
}

void DialogRender::on_horizontalSliderZ_valueChanged(int value)
{
	updateRender();
}

void DialogRender::on_doubleSpinBox_valueChanged(double arg1)
{
	updateRender();
}
