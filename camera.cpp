#include "camera.h"

Camera::Camera()
{
	pov = vec3(0,0,1);
	poi = vec3(0,0,0);
	fov = 45.0f;
}

void Camera::setPointOfView(vec3 _pov)
{
	pov = _pov;
	calculateUVW();
}

void Camera::setPointOfInterest(vec3 _poi)
{
	poi = _poi;
	calculateUVW();
}

void Camera::setFieldOfView(float _fov)
{
	fov = _fov;
	calculateUVW();
}

void Camera::calculateUVW()
{
	w = normalize(negate(poi-pov));
	u = normalize(cross(vec3(0,1,0),w));
	v = cross(w,u);
}

vec3 Camera::visualPixel(float i, float j, int width, int height)
{
	float a,b,d,x,y;

	d = sqrtf(powf(pov.x-poi.x,2) + powf(pov.y-poi.y,2) + powf(pov.z-poi.z,2));

	b = 2*tan((fov/2)*PI/180)*d;
	a = ((float)width/(float)height)*b;

	x =  ((a/width)*(i+0.5))-(a/2);
	y = ((b/height)*(j+0.5))-(b/2);

	return normalize((poi-pov) + u*x + v*y);
}
