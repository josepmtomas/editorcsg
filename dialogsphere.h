#ifndef DIALOGSPHERE_H
#define DIALOGSPHERE_H

#include <QDialog>
#include <QMessageBox>
#include "dialogtransformation.h"
#include "csgtree.h"
#include "sphere.h"

namespace Ui {
class DialogSphere;
}

class DialogSphere : public QDialog
{
    Q_OBJECT
    
public:

    Sphere  *sphere;
    CSGTree *tree;
    bool    *result;
	Sphere	localSphere;
	QString initialName;
	bool	editing;

    explicit DialogSphere(QWidget *parent = 0);
	DialogSphere(QWidget *parent, CSGTree *_tree, Sphere *_sphere, bool *_result, bool _editing);
    ~DialogSphere();

    void updateValues();

	void updateTransformationsList();
    void accept();
    
private slots:

    void on_buttonBox_accepted();
    void on_buttonBox_rejected();

    void on_centerXSpinBox_valueChanged(double arg1);
    void on_centerYSpinBox_valueChanged(double arg1);
    void on_centerZSpinBox_valueChanged(double arg1);

    void on_radiusSpinBox_valueChanged(double arg1);

	void on_nameLineEdit_textChanged(QString arg1);

	void on_addPushButton_clicked();
	void on_deletePushButton_clicked();
	void on_editPushButton_clicked();
	void on_upPushButton_clicked();
	void on_downPushButton_clicked();

private:
    Ui::DialogSphere *ui;
};

#endif // DIALOGSPHERE_H
