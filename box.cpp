#include "box.h"

Box::Box()
{
    type = NodeType::BOX;
	leftChild = NULL;
	rightChild = NULL;
	leftIndex = -1;
	rightIndex = -1;
	leftId = -1;
	rightId = -1;

    width = 1;
    height = 1;
    depth = 1;

    vertices[0] = vec3(-0.5, -0.5,  0.5);
    vertices[1] = vec3( 0.5, -0.5,  0.5);
    vertices[2] = vec3( 0.5, -0.5, -0.5);
    vertices[3] = vec3(-0.5, -0.5, -0.5);
    vertices[4] = vec3(-0.5,  0.5,  0.5);
    vertices[5] = vec3( 0.5,  0.5,  0.5);
    vertices[6] = vec3( 0.5,  0.5, -0.5);
    vertices[7] = vec3(-0.5,  0.5, -0.5);

    faces[0] = u_int4(4,0,1,5);
    faces[1] = u_int4(5,1,2,6);
    faces[2] = u_int4(6,2,3,7);
    faces[3] = u_int4(7,3,0,4);
    faces[4] = u_int4(7,4,5,6);
    faces[5] = u_int4(0,3,2,1);

    fnormals[0] = normalize(cross(vertices[faces[0].y] - vertices[faces[0].x], vertices[faces[0].z] - vertices[faces[0].y]));
    fnormals[1] = normalize(cross(vertices[faces[1].y] - vertices[faces[1].x], vertices[faces[1].z] - vertices[faces[1].y]));
    fnormals[2] = normalize(cross(vertices[faces[2].y] - vertices[faces[2].x], vertices[faces[2].z] - vertices[faces[2].y]));
    fnormals[3] = normalize(cross(vertices[faces[3].y] - vertices[faces[3].x], vertices[faces[3].z] - vertices[faces[3].y]));
    fnormals[4] = normalize(cross(vertices[faces[4].y] - vertices[faces[4].x], vertices[faces[4].z] - vertices[faces[4].y]));
    fnormals[5] = normalize(cross(vertices[faces[5].y] - vertices[faces[5].x], vertices[faces[5].z] - vertices[faces[5].y]));

    tangents[0] = vec3(0.0,1,0);
    tangents[1] = vec3(0.0,1,0);
    tangents[2] = vec3(0.0,1,0);
    tangents[3] = vec3(0.0,1,0);
    tangents[4] = vec3(1.0,0,0);
    tangents[5] = vec3(1.0,0,0);
}

void Box::build(vec3 size)
{
    vertices[0] = vertices[0] * size;
    vertices[1] = vertices[1] * size;
    vertices[2] = vertices[2] * size;
    vertices[3] = vertices[3] * size;
    vertices[4] = vertices[4] * size;
    vertices[5] = vertices[5] * size;
    vertices[6] = vertices[6] * size;
    vertices[7] = vertices[7] * size;

    width = width * size.x;
    height = height * size.y;
    depth = depth * size.z;
}

void Box::rebuild(vec3 size)
{
	type = NodeType::BOX;

    vertices[0] = vec3(-0.5, -0.5,  0.5);
    vertices[1] = vec3( 0.5, -0.5,  0.5);
    vertices[2] = vec3( 0.5, -0.5, -0.5);
    vertices[3] = vec3(-0.5, -0.5, -0.5);
    vertices[4] = vec3(-0.5,  0.5,  0.5);
    vertices[5] = vec3( 0.5,  0.5,  0.5);
    vertices[6] = vec3( 0.5,  0.5, -0.5);
    vertices[7] = vec3(-0.5,  0.5, -0.5);

    vertices[0] = vertices[0] * size;
    vertices[1] = vertices[1] * size;
    vertices[2] = vertices[2] * size;
    vertices[3] = vertices[3] * size;
    vertices[4] = vertices[4] * size;
    vertices[5] = vertices[5] * size;
    vertices[6] = vertices[6] * size;
    vertices[7] = vertices[7] * size;

    width = size.x;
    height = size.y;
    depth = size.z;
}

void Box::recalculateNormalsAndTangents()
{
	for(int i=0; i<6; i++)
		{
			fnormals[i] = normalize(cross(vertices[faces[i].y] - vertices[faces[i].x], vertices[faces[i].z] - vertices[faces[i].y]));
		}

		for(int i=0; i<6; i++)
		{
			if(fnormals[i] == vec3(0.0,1.0,0.0))
			{
				tangents[i] = vec3(1.0,0.0,0.0);
			}
			else if(fnormals[i] == vec3(0.0,-1.0,0.0))
			{
				tangents[i] = vec3(1.0,0.0,0.0);
			}
			else
			{
				tangents[i] = normalize(cross(fnormals[i], vec3(0,1,0)));
			}
		}
}

QString Box::getListString()
{
    QString nameString;
    QString numberString;

    nameString.append("[BOX] ");
    nameString.append(name);
	nameString.append(": ");

	nameString.append(getDescription());

    return nameString;
}

QString Box::getDescription()
{
	QString nameString;
	QString numberString;

	nameString.append("Tamaño (");

	numberString.setNum(width,'g',4);
	nameString.append(numberString);
	nameString.append(", ");

	numberString.setNum(height,'g',4);
	nameString.append(numberString);
	nameString.append(", ");

	numberString.setNum(depth,'g',4);
	nameString.append(numberString);
	nameString.append(")");

	return nameString;
}

void Box::propagateTransformations()
{
	Transformation t;

	for(int i=0; i<transformList.size(); i++)
	{
		t = transformList[i];

		if(t.type == TransformationType::TRANSLATION)
		{
			translation(t.vectorValue);
		}
		else if(t.type == TransformationType::ROTATION)
		{
			rotation(t.vectorValue, t.floatValue);
		}
		else if(t.type == TransformationType::SCALE)
		{
			scale(t.vectorValue);
		}
	}
}

void Box::translation(vec3 amount)
{
	for(int i=0; i<8; i++)
	{
		vertices[i] += amount;
	}
}


void Box::rotation(vec3 rotationAxis, float rads)
{
	for(int i=0; i<8; i++)
	{
		vertices[i] = rotate(vertices[i], rotationAxis, rads);
	}

	recalculateNormalsAndTangents();
}


void Box::scale(vec3 factor)
{
	for(int i=0; i<8; i++)
	{
		vertices[i] = vertices[i] * factor;
	}

	recalculateNormalsAndTangents();
}

bool Box::rayIntersection(vec3 origin, vec3 ray, int numPrimitives, RayIntersection &intersection)
{
	return false;
}

bool Box::rayIntersection(vec3 origin, vec3 ray, RayIntersection *intersections, int2 *arrayIndex)
{
	return false;
}

bool Box::rayIntersection(vec3 origin, vec3 ray, RayIntersection &in, RayIntersection &out)
{
	float te = 0.0f;
	float ts = INFINITE;
	float t = 0.0f;
	float RdotN;

	vec3 normalIn;
	vec3 normalOut;
	vec3 N;

	for(int i=0; i<6; i++)
	{
		//N = normalize(cross(vertices[faces[i].y] - vertices[faces[i].x], vertices[faces[i].z] - vertices[faces[i].y]));
		N = fnormals[i];
		RdotN = dot(N,ray);

		t = dot(N, vertices[faces[i].x] - origin) / RdotN;

		if(RdotN == 0)
		{
			if(t < 0.0f)
			{
				return false;
			}
		}
		else
		{
			if(RdotN < 0)	// IN
			{
				//te = max(te,t);
				if(t > te)
					te = t;

				if(te == t)
				{
					normalIn = N;
				}
			}
			if(RdotN > 0)
			{
				//ts = min(ts,t);
				if(t < ts)
					ts = t;

				if(ts == t)
					normalOut = N;
			}
		}
	}
	if(te <= ts)
	{
		in.distance		= te;
		in.type			= INTERSECTION_IN;
		//in.point		= origin + ray*te;
		in.normal		= normalIn;
		//in.objectType	= type;
		//in.objectIndex	= index;

		out.distance	= ts;
		out.type		= INTERSECTION_OUT;
		//out.point		= origin + ray*ts;
		out.normal		= normalOut;
		//out.objectType	= type;
		//out.objectIndex = index;

		return true;
	}
	else
	{
		return false;
	}
}
