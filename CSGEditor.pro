#-------------------------------------------------
#
# Project created by QtCreator 2014-05-12T12:34:25
#
#-------------------------------------------------

QT       += core gui
QT       += opengl

QMAKE_CXXFLAGS += -fopenmp
LIBS += -fopenmp

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = CSGEditor
TEMPLATE = app

SOURCES += main.cpp\
        mainwindow.cpp \
    dialogcylinder.cpp \
    cylinder.cpp \
    csgnode.cpp \
    transformation.cpp \
    sphere.cpp \
    union.cpp \
    dialogsphere.cpp \
    csgtree.cpp \
    box.cpp \
    dialogbox.cpp \
    dialogtransformation.cpp \
    dialogoperation.cpp \
    difference.cpp \
    intersection.cpp \
    csgfile.cpp \
    dialogprogress.cpp \
    dialogrender.cpp \
    glwidget.cpp \
    rayintersection.cpp \
    camera.cpp

HEADERS  += mainwindow.h \
    dialogcylinder.h \
    cylinder.h \
    Algebra.h \
    csgnode.h \
    transformation.h \
    sphere.h \
    union.h \
    dialogsphere.h \
    csgtree.h \
    box.h \
    dialogbox.h \
    dialogtransformation.h \
    dialogoperation.h \
    difference.h \
    intersection.h \
    csgfile.h \
    dialogprogress.h \
    dialogrender.h \
    glwidget.h \
    rayintersection.h \
    camera.h

FORMS    += mainwindow.ui \
    dialogcylinder.ui \
    dialogsphere.ui \
    dialogbox.ui \
    dialogtransformation.ui \
    dialogoperation.ui \
    dialogprogress.ui \
    dialogrender.ui

RESOURCES += \
    Resources.qrc
