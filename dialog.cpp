#include "dialog.h"
#include "ui_dialog.h"

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);
}

/*Dialog::Dialog(int *parameter)
{
    ui->setupUi(this);
    *parameter = *parameter +1;
}*/

Dialog::Dialog(QWidget *parent, int *i) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    *i = *i +1;
    ui->setupUi(this);
}

Dialog::~Dialog()
{
    delete ui;
}
