#ifndef DIALOGTRANSFORMATION_H
#define DIALOGTRANSFORMATION_H

#include <QDialog>
#include <QMessageBox>
#include "transformation.h"

namespace Ui {
class DialogTransformation;
}

class DialogTransformation : public QDialog
{
    Q_OBJECT
    
public:

    Transformation  *transformation;
    bool            *result;
    int             currentTab;
	vec3			axis;

    explicit DialogTransformation(QWidget *parent = 0);
    DialogTransformation(QWidget *parent, Transformation *transformation, bool *result, bool editing);
    ~DialogTransformation();
    
    void accept();

private slots:

    void on_buttonBox_accepted();
    void on_buttonBox_rejected();

    void on_tabWidget_currentChanged(int index);

private:
    Ui::DialogTransformation *ui;
};

#endif // DIALOGTRANSFORMATION_H
