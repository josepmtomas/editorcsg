#include "dialogprogress.h"
#include "ui_dialogprogress.h"

DialogProgress::DialogProgress(QWidget *parent) :
	QDialog(parent),
	ui(new Ui::DialogProgress)
{
	ui->setupUi(this);
}

DialogProgress::DialogProgress(QWidget *parent, QString file, CSGTree *tree, bool saving) :
	QDialog(parent),
	ui(new Ui::DialogProgress)
{
	this->file = file;
	this->tree = tree;
	this->saving = saving;

	ui->setupUi(this);

	if(saving)
	{
		save();
	}
	else
	{
		load();
		buildTreeRoots();
	}

	QApplication::postEvent(this,new QCloseEvent());
}

DialogProgress::~DialogProgress()
{
	delete ui;
}

void DialogProgress::save()
{
	bool valid = treeIsValid();
	CSGNode *node;
	Sphere	*sphere;
	Box		*box;
	Cylinder *cylinder;
	FILE	*outFile;

	if(valid) qDebug("The tree is valid");
	else qDebug("The tree is not valid");

	outFile = fopen(file.toStdString().c_str(),"w");
	if(!outFile)
	{
		qDebug("Failed to open the file for writing");
	}
	else
	{

		//////////////////////////////////////////////////////////////////////////////////
		/// Tree info
		//////////////////////////////////////////////////////////////////////////////////

		fprintf(outFile,"Tree\n{\n");

		if(valid)	fprintf(outFile,"\tValid True\n");
		else		fprintf(outFile,"\tValid False\n");

		fprintf(outFile,"}\n\n");

		//////////////////////////////////////////////////////////////////////////////////
		/// Nodes
		//////////////////////////////////////////////////////////////////////////////////

		for(int i=0; i<tree->nodes.size(); i++)
		{
			node = tree->nodes[i];

			if(node->type == NodeType::OP_UNION ||
			   node->type == NodeType::OP_DIFFERENCE ||
			   node->type == NodeType::OP_INTERSECTION)
			{
				if(node->type == NodeType::OP_UNION)	fprintf(outFile,"Union\n{\n");
				else if(node->type == NodeType::OP_DIFFERENCE)	fprintf(outFile, "Difference\n{\n");
				else if(node->type == NodeType::OP_INTERSECTION)	fprintf(outFile, "Intersection\n{\n");

				fprintf(outFile,"\tId %d\n", node->id);
				fprintf(outFile,"\tName \"%s\"\n", node->name.toStdString().c_str());
				fprintf(outFile,"\tParent %d\n", node->parent);

				if(node->leftChild)	fprintf(outFile,"\tLeftChild %d\n", node->leftChild->id);
				else				fprintf(outFile,"\tLeftChild -1\n");

				if(node->rightChild)fprintf(outFile,"\tRightChild %d\n", node->rightChild->id);
				else				fprintf(outFile,"\tRightChild -1\n");

				writeTransformations(outFile,node);

				fprintf(outFile,"}\n\n");
			}
			else if(node->type == NodeType::BOX)
			{
				box = (Box*)node;

				fprintf(outFile,"Box\n{\n");
				fprintf(outFile,"\tId %d\n", box->id);
				fprintf(outFile,"\tName \"%s\"\n", box->name.toStdString().c_str());
				fprintf(outFile,"\tParent %d\n", box->parent);

				fprintf(outFile,"\tWidth %.4f\n", box->width);
				fprintf(outFile,"\tHeight %.4f\n", box->height);
				fprintf(outFile,"\tDepth %.4f\n", box->depth);

				writeTransformations(outFile,box);

				fprintf(outFile,"}\n\n");
			}
			else if(node->type == NodeType::CYLINDER)
			{
				cylinder = (Cylinder*)node;

				fprintf(outFile,"Cylinder\n{\n");
				fprintf(outFile,"\tId %d\n", cylinder->id);
				fprintf(outFile,"\tName \"%s\"\n", cylinder->name.toStdString().c_str());
				fprintf(outFile,"\tParent %d\n", cylinder->parent);

				fprintf(outFile,"\tBase %.4f %.4f %.4f\n", cylinder->base.x, cylinder->base.y, cylinder->base.z);
				fprintf(outFile,"\tCenter %.4f %.4f %.4f\n", cylinder->center.x, cylinder->center.y, cylinder->center.z);
				fprintf(outFile,"\tAxis %.4f %.4f %.4f\n", cylinder->axis.x, cylinder->axis.y, cylinder->axis.z);
				fprintf(outFile,"\tRadius %.4f\n", cylinder->radius);
				fprintf(outFile,"\tHeight %.4f\n", cylinder->height);

				writeTransformations(outFile, cylinder);

				fprintf(outFile,"}\n\n");
			}
			else if(node->type == NodeType::SPHERE)
			{
				sphere = (Sphere*)node;

				fprintf(outFile,"Sphere\n{\n");
				fprintf(outFile,"\tId %d\n", sphere->id);
				fprintf(outFile,"\tName \"%s\"\n", sphere->name.toStdString().c_str());
				fprintf(outFile,"\tParent %d\n", sphere->parent);

				fprintf(outFile,"\tCenter %.4f %.4f %.4f\n", sphere->center.x, sphere->center.y, sphere->center.z);
				fprintf(outFile,"\tRadius %.4f\n", sphere->radius);

				writeTransformations(outFile, sphere);

				fprintf(outFile,"}\n\n");
			}

		}

		fclose(outFile);
	}
}

void DialogProgress::load()
{
	int lineNumber = 0;
	bool omitLine;
	bool omitGroupLine;
	bool endGroup;
	std::string line;
	std::string token;
	std::stringstream lineStream;

	QString errorString;
	Transformation transformation;

	//////////////////////////////////////////////////////

	Union			*opUnion;
	Difference		*opDifference;
	Intersection	*opIntersection;

	Box			*box;
	Cylinder	*cylinder;
	Sphere		*sphere;

	/////////////////////////////////////////////////////

	FILE *logFile = fopen("importLog.txt","w");
	if(!logFile)
	{
		QMessageBox::critical(this,"Error","Error al abrir e archivo de informe");
		DialogProgress::close();
	}

	std::ifstream inFile(file.toStdString().c_str());

	if(!inFile.is_open())
	{
		QMessageBox::critical(this,"Error","Error al abrir el archivo");
		DialogProgress::close();
	}
	else
	{
		while(!inFile.eof())
		{
			lineNumber++;
			omitLine = false;
			std::getline(inFile,line);
			lineStream = std::stringstream(line);

			while(lineStream >> token && !omitLine)
			{
				/////////////////////////////////////////////////////
				/// TREE INFO PARSE
				/////////////////////////////////////////////////////

				if(token.compare("Tree") == 0)
				{
					endGroup = false;
					omitGroupLine = false;

					std::getline(inFile,line);
					lineStream = std::stringstream(line);
					lineStream >> token;
					lineNumber++;

					if(token.compare("{") != 0)
					{
						fprintf(logFile,"Line %d: unexpected token '%s'\n",lineNumber, token.c_str());
						QMessageBox::critical(this,"Error","El archivo de entrada no es válido");
						DialogProgress::close();
					}

					while(!endGroup)
					{
						omitGroupLine = false;
						std::getline(inFile,line);
						lineNumber++;
						lineStream = std::stringstream(line);

						while(lineStream >> token && !omitGroupLine)
						{
							omitGroupLine = false;

							if(token.compare("Valid") == 0)
							{
								if(lineStream >> token)
								{}
								else
								{
									fprintf(logFile,"Line %d: incomplete field\n",lineNumber);
									QMessageBox::critical(this,"Error","El archivo de entrada no es válido");
									DialogProgress::close();
								}
								omitGroupLine = true;
							}
							else if(token.compare("}") == 0)
							{
								omitGroupLine = true;
								endGroup = true;
							}
							/*else
							{
								fprintf(logFile,"Line %d: unexpected token '%s'\n",lineNumber, token.c_str());
								QMessageBox::critical(this,"Error","El archivo de entrada no es válido");
								DialogProgress::close();
							}*/
						}
					}

					fprintf(logFile,"Read tree status\n");
				}

				/////////////////////////////////////////////////////
				/// UNION PARSE
				/////////////////////////////////////////////////////

				else if(token.compare("Union") == 0)
				{
					endGroup = false;
					omitGroupLine = false;
					opUnion = new Union();

					std::getline(inFile,line);
					lineStream = std::stringstream(line);
					lineStream >> token;
					lineNumber++;

					if(token.compare("{") != 0)
					{
						fprintf(logFile,"Line %d: unexpected token '%s'\n",lineNumber, token.c_str());
						QMessageBox::critical(this,"Error","El archivo de entrada no es válido");
						DialogProgress::close();
					}

					while(!endGroup)
					{
						omitGroupLine = false;
						lineNumber++;
						std::getline(inFile,line);
						lineStream = std::stringstream(line);

						while(lineStream >> token && !omitGroupLine)
						{
							if(token.compare("}") == 0)
							{
								omitGroupLine = true;
								endGroup = true;
							}

							else if(token.compare("Id") == 0)
							{
								if(lineStream >> token)
									opUnion->id = atoi(token.c_str());
								else
								{
									fprintf(logFile,"Line %d: incomplete field\n",lineNumber);
									QMessageBox::critical(this,"Error","El archivo de entrada no es válido");
									DialogProgress::close();
								}
							}

							else if(token.compare("Name") == 0)
							{
								std::getline(lineStream,token,'"');
								std::getline(lineStream,token,'"');
								opUnion->name = QString(token.c_str());
							}

							else if(token.compare("Parent") == 0)
							{
								lineStream >> token;
								opUnion->parent = atoi(token.c_str());
								qDebug("IMPORTING TREE | UNION | PARENT ID = %d", opUnion->parent);
							}

							else if(token.compare("LeftChild") == 0)
							{
								lineStream >> token;
								opUnion->leftId = atoi(token.c_str());
								qDebug("IMPORTING TREE | UNION | LEFT  ID = %d - %d", opUnion->leftId, atoi(token.c_str()));
							}

							else if(token.compare("RightChild") == 0)
							{
								lineStream >> token;
								opUnion->rightId = atoi(token.c_str());
								qDebug("IMPORTING TREE | UNION | RIGHT ID = %d - %d", opUnion->rightId, atoi(token.c_str()));
							}

							else if(token.compare("Translation") == 0)
							{
								transformation = Transformation();
								transformation.type = TransformationType::TRANSLATION;
								lineStream >> token;	transformation.vectorValue.x = (float)atof(token.c_str());
								lineStream >> token;	transformation.vectorValue.y = (float)atof(token.c_str());
								lineStream >> token;	transformation.vectorValue.z = (float)atof(token.c_str());
								opUnion->transformList.push_back(transformation);
							}

							else if(token.compare("Rotation") == 0)
							{
								transformation = Transformation();
								transformation.type = TransformationType::ROTATION;
								lineStream >> token;	transformation.vectorValue.x = (float)atof(token.c_str());
								lineStream >> token;	transformation.vectorValue.y = (float)atof(token.c_str());
								lineStream >> token;	transformation.vectorValue.z = (float)atof(token.c_str());
								lineStream >> token;	transformation.floatValue = (float)atof(token.c_str());
								opUnion->transformList.push_back(transformation);
							}

							else if(token.compare("Scale") == 0)
							{
								transformation = Transformation();
								transformation.type = TransformationType::SCALE;
								lineStream >> token;	transformation.vectorValue.x = (float)atof(token.c_str());
								lineStream >> token;	transformation.vectorValue.y = (float)atof(token.c_str());
								lineStream >> token;	transformation.vectorValue.z = (float)atof(token.c_str());
								opUnion->transformList.push_back(transformation);
							}

							/*else
							{
								fprintf(logFile,"Line %d: unexpected token '%s'\n",lineNumber, token.c_str());
								QMessageBox::critical(this,"Error","El archivo de entrada no es válido");
								DialogProgress::close();
							}*/
						}
					}

					tree->addNode(opUnion,true);

					fprintf(logFile,"Read Union\n");
				}

				/////////////////////////////////////////////////////
				/// DIFFERENCE PARSE
				/////////////////////////////////////////////////////

				else if(token.compare("Difference") == 0)
				{
					endGroup = false;
					omitGroupLine = false;
					opDifference = new Difference();

					std::getline(inFile,line);
					lineStream = std::stringstream(line);
					lineStream >> token;
					lineNumber++;

					if(token.compare("{") != 0)
					{
						fprintf(logFile,"Line %d: unexpected token '%s'\n",lineNumber, token.c_str());
						QMessageBox::critical(this,"Error","El archivo de entrada no es válido");
						DialogProgress::close();
					}

					while(!endGroup)
					{
						omitGroupLine = false;
						lineNumber++;
						std::getline(inFile,line);
						lineStream = std::stringstream(line);

						while(lineStream >> token && !omitGroupLine)
						{
							if(token.compare("}") == 0)
							{
								omitGroupLine = true;
								endGroup = true;
							}

							else if(token.compare("Id") == 0)
							{
								if(lineStream >> token)
									opDifference->id = atoi(token.c_str());
								else
								{
									fprintf(logFile,"Line %d: incomplete field\n",lineNumber);
									QMessageBox::critical(this,"Error","El archivo de entrada no es válido");
									DialogProgress::close();
								}
							}

							else if(token.compare("Name") == 0)
							{
								std::getline(lineStream,token,'"');
								std::getline(lineStream,token,'"');
								opDifference->name = QString(token.c_str());
							}

							else if(token.compare("Parent") == 0)
							{
								lineStream >> token;
								opDifference->parent = atoi(token.c_str());
							}

							else if(token.compare("LeftChild") == 0)
							{
								lineStream >> token;
								opDifference->leftId = atoi(token.c_str());
							}

							else if(token.compare("RightChild") == 0)
							{
								lineStream >> token;
								opDifference->rightId = atoi(token.c_str());
							}

							else if(token.compare("Translation") == 0)
							{
								transformation = Transformation();
								transformation.type = TransformationType::TRANSLATION;
								lineStream >> token;	transformation.vectorValue.x = (float)atof(token.c_str());
								lineStream >> token;	transformation.vectorValue.y = (float)atof(token.c_str());
								lineStream >> token;	transformation.vectorValue.z = (float)atof(token.c_str());
								opDifference->transformList.push_back(transformation);
							}

							else if(token.compare("Rotation") == 0)
							{
								transformation = Transformation();
								transformation.type = TransformationType::ROTATION;
								lineStream >> token;	transformation.vectorValue.x = (float)atof(token.c_str());
								lineStream >> token;	transformation.vectorValue.y = (float)atof(token.c_str());
								lineStream >> token;	transformation.vectorValue.z = (float)atof(token.c_str());
								lineStream >> token;	transformation.floatValue = (float)atof(token.c_str());
								opDifference->transformList.push_back(transformation);
							}

							else if(token.compare("Scale") == 0)
							{
								transformation = Transformation();
								transformation.type = TransformationType::SCALE;
								lineStream >> token;	transformation.vectorValue.x = (float)atof(token.c_str());
								lineStream >> token;	transformation.vectorValue.y = (float)atof(token.c_str());
								lineStream >> token;	transformation.vectorValue.z = (float)atof(token.c_str());
								opDifference->transformList.push_back(transformation);
							}

							/*else
							{
								fprintf(logFile,"Line %d: unexpected token '%s'\n",lineNumber, token.c_str());
								QMessageBox::critical(this,"Error","El archivo de entrada no es válido");
								DialogProgress::close();
							}*/
						}
					}

					tree->addNode(opDifference,true);

					fprintf(logFile,"Read difference\n");
				}

				/////////////////////////////////////////////////////
				/// INTERSECION PARSE
				/////////////////////////////////////////////////////

				else if(token.compare("Intersection") == 0)
				{
					endGroup = false;
					omitGroupLine = false;
					opIntersection = new Intersection();

					std::getline(inFile,line);
					lineStream = std::stringstream(line);
					lineStream >> token;
					lineNumber++;

					if(token.compare("{") != 0)
					{
						fprintf(logFile,"Line %d: unexpected token '%s'\n",lineNumber, token.c_str());
						QMessageBox::critical(this,"Error","El archivo de entrada no es válido");
						DialogProgress::close();
					}

					while(!endGroup)
					{
						omitGroupLine = false;
						lineNumber++;
						std::getline(inFile,line);
						lineStream = std::stringstream(line);

						while(lineStream >> token && !omitGroupLine)
						{
							if(token.compare("}") == 0)
							{
								omitGroupLine = true;
								endGroup = true;
							}

							else if(token.compare("Id") == 0)
							{
								if(lineStream >> token)
									opIntersection->id = atoi(token.c_str());
								else
								{
									fprintf(logFile,"Line %d: incomplete field\n",lineNumber);
									QMessageBox::critical(this,"Error","El archivo de entrada no es válido");
									DialogProgress::close();
								}
							}

							else if(token.compare("Name") == 0)
							{
								std::getline(lineStream,token,'"');
								std::getline(lineStream,token,'"');
								opIntersection->name = QString(token.c_str());
							}

							else if(token.compare("Parent") == 0)
							{
								lineStream >> token;
								opIntersection->parent = atoi(token.c_str());
							}

							else if(token.compare("LeftChild") == 0)
							{
								lineStream >> token;
								opIntersection->leftId = atoi(token.c_str());
							}

							else if(token.compare("RightChild") == 0)
							{
								lineStream >> token;
								opIntersection->rightId = atoi(token.c_str());
							}

							else if(token.compare("Translation") == 0)
							{
								transformation = Transformation();
								transformation.type = TransformationType::TRANSLATION;
								lineStream >> token;	transformation.vectorValue.x = (float)atof(token.c_str());
								lineStream >> token;	transformation.vectorValue.y = (float)atof(token.c_str());
								lineStream >> token;	transformation.vectorValue.z = (float)atof(token.c_str());
								opIntersection->transformList.push_back(transformation);
							}

							else if(token.compare("Rotation") == 0)
							{
								transformation = Transformation();
								transformation.type = TransformationType::ROTATION;
								lineStream >> token;	transformation.vectorValue.x = (float)atof(token.c_str());
								lineStream >> token;	transformation.vectorValue.y = (float)atof(token.c_str());
								lineStream >> token;	transformation.vectorValue.z = (float)atof(token.c_str());
								lineStream >> token;	transformation.floatValue = (float)atof(token.c_str());
								opIntersection->transformList.push_back(transformation);
							}

							else if(token.compare("Scale") == 0)
							{
								transformation = Transformation();
								transformation.type = TransformationType::SCALE;
								lineStream >> token;	transformation.vectorValue.x = (float)atof(token.c_str());
								lineStream >> token;	transformation.vectorValue.y = (float)atof(token.c_str());
								lineStream >> token;	transformation.vectorValue.z = (float)atof(token.c_str());
								opIntersection->transformList.push_back(transformation);
							}

							/*else
							{
								fprintf(logFile,"Line %d: unexpected token '%s'\n",lineNumber, token.c_str());
								QMessageBox::critical(this,"Error","El archivo de entrada no es válido");
								DialogProgress::close();
							}*/
						}
					}

					tree->addNode(opIntersection,true);

					fprintf(logFile,"Read intersection\n");
				}

				/////////////////////////////////////////////////////
				/// SPHERE PARSE
				/////////////////////////////////////////////////////

				else if(token.compare("Sphere") == 0)
				{
					endGroup = false;
					omitGroupLine = false;
					sphere = new Sphere();

					std::getline(inFile,line);
					lineStream = std::stringstream(line);
					lineStream >> token;
					lineNumber++;

					if(token.compare("{") != 0)
					{
						fprintf(logFile,"Line %d: unexpected token '%s'\n",lineNumber, token.c_str());
						QMessageBox::critical(this,"Error","El archivo de entrada no es válido");
						DialogProgress::close();
					}

					while(!endGroup)
					{
						omitGroupLine = false;
						lineNumber++;
						std::getline(inFile,line);
						lineStream = std::stringstream(line);

						while(lineStream >> token && !omitGroupLine)
						{
							if(token.compare("}") == 0)
							{
								omitGroupLine = true;
								endGroup = true;
							}

							else if(token.compare("Id") == 0)
							{
								if(lineStream >> token)
									sphere->id = atoi(token.c_str());
								else
								{
									fprintf(logFile,"Line %d: incomplete field\n",lineNumber);
									QMessageBox::critical(this,"Error","El archivo de entrada no es válido");
									DialogProgress::close();
								}
							}

							else if(token.compare("Name") == 0)
							{

								std::getline(lineStream,token,'"');
								std::getline(lineStream,token,'"');
								sphere->name = QString(token.c_str());
							}

							else if(token.compare("Parent") == 0)
							{
								lineStream >> token;
								sphere->parent = atoi(token.c_str());
							}

							else if(token.compare("Center") == 0)
							{
								lineStream >> token; sphere->center.x = (float)atof(token.c_str());
								lineStream >> token; sphere->center.y = (float)atof(token.c_str());
								lineStream >> token; sphere->center.z = (float)atof(token.c_str());
							}

							else if(token.compare("Radius") == 0)
							{
								lineStream >> token;
								sphere->radius = (float)atof(token.c_str());
							}

							else if(token.compare("Translation") == 0)
							{
								transformation = Transformation();
								transformation.type = TransformationType::TRANSLATION;
								lineStream >> token;	transformation.vectorValue.x = (float)atof(token.c_str());
								lineStream >> token;	transformation.vectorValue.y = (float)atof(token.c_str());
								lineStream >> token;	transformation.vectorValue.z = (float)atof(token.c_str());
								sphere->transformList.push_back(transformation);
							}

							else if(token.compare("Rotation") == 0)
							{
								transformation = Transformation();
								transformation.type = TransformationType::ROTATION;
								lineStream >> token;	transformation.vectorValue.x = (float)atof(token.c_str());
								lineStream >> token;	transformation.vectorValue.y = (float)atof(token.c_str());
								lineStream >> token;	transformation.vectorValue.z = (float)atof(token.c_str());
								lineStream >> token;	transformation.floatValue = (float)atof(token.c_str());
								sphere->transformList.push_back(transformation);
							}

							else if(token.compare("Scale") == 0)
							{
								transformation = Transformation();
								transformation.type = TransformationType::SCALE;
								lineStream >> token;	transformation.vectorValue.x = (float)atof(token.c_str());
								lineStream >> token;	transformation.vectorValue.y = (float)atof(token.c_str());
								lineStream >> token;	transformation.vectorValue.z = (float)atof(token.c_str());
								sphere->transformList.push_back(transformation);
							}

							/*else
							{
								fprintf(logFile,"Line %d: unexpected token '%s'\n",lineNumber, token.c_str());
								QMessageBox::critical(this,"Error","El archivo de entrada no es válido");
								DialogProgress::close();
							}*/
						}
					}
					tree->addNode(sphere,true);

					fprintf(logFile,"Read sphere\n");
				}

				/////////////////////////////////////////////////////
				/// BOX PARSE
				/////////////////////////////////////////////////////

				else if(token.compare("Box") == 0)
				{
					endGroup = false;
					omitGroupLine = false;
					box = new Box();

					std::getline(inFile,line);
					lineStream = std::stringstream(line);
					lineStream >> token;
					lineNumber++;

					if(token.compare("{") != 0)
					{
						fprintf(logFile,"Line %d: unexpected token '%s'\n",lineNumber, token.c_str());
						QMessageBox::critical(this,"Error","El archivo de entrada no es válido");
						DialogProgress::close();
					}

					while(!endGroup)
					{
						omitGroupLine = false;
						lineNumber++;
						std::getline(inFile,line);
						lineStream = std::stringstream(line);

						while(lineStream >> token && !omitGroupLine)
						{
							if(token.compare("}") == 0)
							{
								omitGroupLine = true;
								endGroup = true;
							}

							else if(token.compare("Id") == 0)
							{
								if(lineStream >> token)
									box->id = atoi(token.c_str());
								else
								{
									fprintf(logFile,"Line %d: incomplete field\n",lineNumber);
									QMessageBox::critical(this,"Error","El archivo de entrada no es válido");
									DialogProgress::close();
								}
							}

							else if(token.compare("Name") == 0)
							{

								std::getline(lineStream,token,'"');
								std::getline(lineStream,token,'"');
								box->name = QString(token.c_str());
							}

							else if(token.compare("Parent") == 0)
							{
								lineStream >> token;
								box->parent = atoi(token.c_str());
							}

							else if(token.compare("Width") == 0)
							{
								lineStream >> token;
								box->width = (float)atof(token.c_str());
							}

							else if(token.compare("Height") == 0)
							{
								lineStream >> token;
								box->height = (float)atof(token.c_str());
							}

							else if(token.compare("Depth") == 0)
							{
								lineStream >> token;
								box->depth = (float)atof(token.c_str());
							}

							else if(token.compare("Translation") == 0)
							{
								transformation = Transformation();
								transformation.type = TransformationType::TRANSLATION;
								lineStream >> token;	transformation.vectorValue.x = (float)atof(token.c_str());
								lineStream >> token;	transformation.vectorValue.y = (float)atof(token.c_str());
								lineStream >> token;	transformation.vectorValue.z = (float)atof(token.c_str());
								box->transformList.push_back(transformation);
							}

							else if(token.compare("Rotation") == 0)
							{
								transformation = Transformation();
								transformation.type = TransformationType::ROTATION;
								lineStream >> token;	transformation.vectorValue.x = (float)atof(token.c_str());
								lineStream >> token;	transformation.vectorValue.y = (float)atof(token.c_str());
								lineStream >> token;	transformation.vectorValue.z = (float)atof(token.c_str());
								lineStream >> token;	transformation.floatValue = (float)atof(token.c_str());
								box->transformList.push_back(transformation);
							}

							else if(token.compare("Scale") == 0)
							{
								transformation = Transformation();
								transformation.type = TransformationType::SCALE;
								lineStream >> token;	transformation.vectorValue.x = (float)atof(token.c_str());
								lineStream >> token;	transformation.vectorValue.y = (float)atof(token.c_str());
								lineStream >> token;	transformation.vectorValue.z = (float)atof(token.c_str());
								box->transformList.push_back(transformation);
							}

							/*else
							{
								fprintf(logFile,"Line %d: unexpected token '%s'\n",lineNumber, token.c_str());
								QMessageBox::critical(this,"Error","El archivo de entrada no es válido");
								DialogProgress::close();
							}*/
						}
					}

					box->rebuild(vec3(box->width, box->height, box->depth));
					tree->addNode(box,true);

					fprintf(logFile,"Read box\n");
				}

				/////////////////////////////////////////////////////
				/// CYLINDER PARSE
				/////////////////////////////////////////////////////

				else if(token.compare("Cylinder") == 0)
				{
					endGroup = false;
					omitGroupLine = false;
					cylinder = new Cylinder();

					std::getline(inFile,line);
					lineStream = std::stringstream(line);
					lineStream >> token;
					lineNumber++;

					if(token.compare("{") != 0)
					{
						fprintf(logFile,"Line %d: unexpected token '%s'\n",lineNumber, token.c_str());
						QMessageBox::critical(this,"Error","El archivo de entrada no es válido");
						DialogProgress::close();
					}

					while(!endGroup)
					{
						omitGroupLine = false;
						lineNumber++;
						std::getline(inFile,line);
						lineStream = std::stringstream(line);

						while(lineStream >> token && !omitGroupLine)
						{
							if(token.compare("}") == 0)
							{
								omitGroupLine = true;
								endGroup = true;
							}

							else if(token.compare("Id") == 0)
							{
								if(lineStream >> token)
									cylinder->id = atoi(token.c_str());
								else
								{
									fprintf(logFile,"Line %d: incomplete field\n",lineNumber);
									QMessageBox::critical(this,"Error","El archivo de entrada no es válido");
									DialogProgress::close();
								}
							}

							else if(token.compare("Name") == 0)
							{

								std::getline(lineStream,token,'"');
								std::getline(lineStream,token,'"');
								cylinder->name = QString(token.c_str());
							}

							else if(token.compare("Parent") == 0)
							{
								lineStream >> token;
								cylinder->parent = atoi(token.c_str());
							}

							else if(token.compare("Base") == 0)
							{
								lineStream >> token;	cylinder->base.x = (float)atof(token.c_str());
								lineStream >> token;	cylinder->base.y = (float)atof(token.c_str());
								lineStream >> token;	cylinder->base.z = (float)atof(token.c_str());
							}

							else if(token.compare("Center") == 0)
							{
								lineStream >> token;	cylinder->center.x = (float)atof(token.c_str());
								lineStream >> token;	cylinder->center.y = (float)atof(token.c_str());
								lineStream >> token;	cylinder->center.z = (float)atof(token.c_str());
							}

							else if(token.compare("Axis") == 0)
							{
								lineStream >> token;	cylinder->axis.x = (float)atof(token.c_str());
								lineStream >> token;	cylinder->axis.y = (float)atof(token.c_str());
								lineStream >> token;	cylinder->axis.z = (float)atof(token.c_str());
							}

							else if(token.compare("Radius") == 0)
							{
								lineStream >> token;
								cylinder->radius = (float)atof(token.c_str());
							}

							else if(token.compare("Height") == 0)
							{
								lineStream >> token;
								cylinder->height = (float)atof(token.c_str());
							}

							else if(token.compare("Translation") == 0)
							{
								transformation = Transformation();
								transformation.type = TransformationType::TRANSLATION;
								lineStream >> token;	transformation.vectorValue.x = (float)atof(token.c_str());
								lineStream >> token;	transformation.vectorValue.y = (float)atof(token.c_str());
								lineStream >> token;	transformation.vectorValue.z = (float)atof(token.c_str());
								cylinder->transformList.push_back(transformation);
							}

							else if(token.compare("Rotation") == 0)
							{
								transformation = Transformation();
								transformation.type = TransformationType::ROTATION;
								lineStream >> token;	transformation.vectorValue.x = (float)atof(token.c_str());
								lineStream >> token;	transformation.vectorValue.y = (float)atof(token.c_str());
								lineStream >> token;	transformation.vectorValue.z = (float)atof(token.c_str());
								lineStream >> token;	transformation.floatValue = (float)atof(token.c_str());
								cylinder->transformList.push_back(transformation);
							}

							else if(token.compare("Scale") == 0)
							{
								transformation = Transformation();
								transformation.type = TransformationType::SCALE;
								lineStream >> token;	transformation.vectorValue.x = (float)atof(token.c_str());
								lineStream >> token;	transformation.vectorValue.y = (float)atof(token.c_str());
								lineStream >> token;	transformation.vectorValue.z = (float)atof(token.c_str());
								cylinder->transformList.push_back(transformation);
							}

							/*else
							{
								fprintf(logFile,"Line %d: unexpected token '%s'\n",lineNumber, token.c_str());
								QMessageBox::critical(this,"Error","El archivo de entrada no es válido");
								DialogProgress::close();
							}*/
						}
					}

					tree->addNode(cylinder,true);

					fprintf(logFile,"Read cylinder\n");
				}
			}
		}
	}

	fflush(logFile);

	inFile.close();
	fclose(logFile);
}

void DialogProgress::buildTreeRoots()
{
	CSGNode *node;
	CSGNode *child;

	for(int i=0; i<tree->nodes.size(); i++)
	{
		node = tree->nodes[i];

		qDebug("IMPORT TREE | NODE [%d] IS A NODE WITH PARENT = %d", i, node->parent);
		if(node->parent == -1)
		{
			qDebug("IMPORT TREE | NODE [%d] | IS A ROOT WITH ID = %d", i, node->id);
			qDebug("IMPORT TREE | NODE [%d] | LEFT  CHILD ID = %d", i, node->leftId);
			qDebug("IMPORT TREE | NODE [%d] | RIGHT CHILD ID = %d", i, node->rightId);
			child = tree->getNode(node->leftId);
			if(child)
			{
				//qDebug("IMPORT_TREE | NODE [%d] HAS A LEFT CHILD WITH ID = %d", i, node->id);
				node->leftChild = child;

				if(child->type == NodeType::OP_UNION ||
					child->type == NodeType::OP_DIFFERENCE ||
					child->type == NodeType::OP_INTERSECTION)
				{
					buildTreeChilds(child);
				}
			}

			child = tree->getNode(node->rightId);
			if(child)
			{
				qDebug("IMPORT_TREE | NODE [%d] HAS A RIGHT CHILD WITH ID = %d", i, node->id);
				node->rightChild = child;

				if(child->type == NodeType::OP_UNION ||
					child->type == NodeType::OP_DIFFERENCE ||
					child->type == NodeType::OP_INTERSECTION)
				{
					buildTreeChilds(child);
				}
			}

		}
	}
}

void DialogProgress::buildTreeChilds(CSGNode *node)
{
	CSGNode *child;

	child = tree->getNode(node->leftId);
	if(child)
	{
		node->leftChild = child;
		if( child->type == NodeType::OP_UNION ||
			child->type == NodeType::OP_DIFFERENCE ||
			child->type == NodeType::OP_INTERSECTION)
		{
			buildTreeChilds(child);
		}
	}

	child = tree->getNode(node->rightId);
	if(child)
	{
		node->rightChild = child;
		if( child->type == NodeType::OP_UNION ||
			child->type == NodeType::OP_DIFFERENCE ||
			child->type == NodeType::OP_INTERSECTION)
		{
			buildTreeChilds(child);
		}
	}
}

bool DialogProgress::treeIsValid()
{
	int rootCount = 0;
	int rootIndex = 0;

	for(int i=0; i<tree->nodes.size(); i++)
	{
		if(tree->nodes[i]->parent == -1)
		{
			rootCount++;
			rootIndex = i;
		}
	}

	if(rootCount > 1 || rootCount == 0)
		return false;
	else
		return treeIsValid(tree->nodes[rootIndex]);
}

bool DialogProgress::treeIsValid(CSGNode *root)
{
	if(root->type == NodeType::OP_UNION ||
		root->type == NodeType::OP_DIFFERENCE ||
		root->type == NodeType::OP_INTERSECTION)
	{
		if(!root->leftChild)
			return false;
		if(!root->rightChild)
			return false;

		return ((treeIsValid(root->leftChild)) && (treeIsValid(root->rightChild)));
	}
	else
	{
		return true;
	}
}

void DialogProgress::writeTransformations(FILE *file, CSGNode *node)
{
	Transformation t;

	for(int i=0; i<node->transformList.size(); i++)
	{
		t = node->transformList[i];
		if(t.type == TransformationType::TRANSLATION)
		{
			fprintf(file,"\tTranslation %.4f %.4f %.4f\n", t.vectorValue.x, t.vectorValue.y, t.vectorValue.z);
		}
		else if(t.type == TransformationType::ROTATION)
		{
			fprintf(file,"\tRotation %.4f %.4f %.4f %.4f\n", t.vectorValue.x, t.vectorValue.y, t.vectorValue.z, t.floatValue);
		}
		else if(t.type == TransformationType::SCALE)
		{
			fprintf(file,"\tScale %.4f %.4f %.4f\n", t.vectorValue.x, t.vectorValue.y, t.vectorValue.z);
		}
	}
}
