#ifndef CSGOPERATION_H
#define CSGOPERATION_H

#include "csgnode.h"

class CSGOperation : public CSGNode
{
public:
    CSGOperation();

    CSGNode *children[2];

    Union();

    void setLeftChildren(CSGNode *node);
    void setRightChildren(CSGNode *node);

    QString getListString();
    void propagateTransformations();
    void translation(vec3 amount);
    void rotation(vec3 rotationAxis, float rads);
    void scale(vec3 factor);
};

#endif // CSGOPERATION_H
