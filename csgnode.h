#ifndef CSGNODE_H
#define CSGNODE_H

#include <QString>
#include <vector>

#include "rayintersection.h"
#include "transformation.h"

enum NodeType
{
	BLANK,
	NODE,
	OP_UNION,
	OP_DIFFERENCE,
	OP_INTERSECTION,
	BOX,
	CYLINDER,
	SPHERE
};

class CSGNode
{
public:

	int			id;
    int         parent;
	int			index;
	int			leftIndex;
	int			rightIndex;
	int			leftId;
	int			rightId;
    QString     name;
    NodeType    type;
    std::vector<Transformation> transformList;
	CSGNode		*leftChild;
	CSGNode		*rightChild;

    CSGNode(void);

    virtual QString getListString();
	virtual QString getDescription();
    virtual void propagateTransformations();
    void translation(vec3 amount);
    void rotation(vec3 rotationAxis, float rads);
    void scale(vec3 factor);

	void recursiveMark(int *_index);
	void toArray(std::vector<CSGNode> *nodes);

	virtual bool rayIntersection(vec3 origin, vec3 ray, int numPrimitives, RayIntersection &intersection);
	virtual bool rayIntersection(vec3 origin, vec3 ray, RayIntersection *intersections, int2 *arrayIndex);
	virtual bool rayIntersection(vec3 origin, vec3 ray, RayIntersection &in, RayIntersection &out);
};

#endif // CSGNODE_H
