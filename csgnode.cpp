#include "csgnode.h"

CSGNode::CSGNode(void)
{
    type = NodeType::NODE;
    parent = -1;
}

QString CSGNode::getListString()
{
    return QString("CSG_Node");
}

QString CSGNode::getDescription()
{
	return QString("Description");
}

void CSGNode::propagateTransformations()
{

}

void CSGNode::translation(vec3 amount)
{
    Transformation t;
    t.type = TransformationType::TRANSLATION;
    t.vectorValue = amount;

    transformList.push_back(t);
}

void CSGNode::rotation(vec3 rotationAxis, float rads)
{
    Transformation t;
    t.type = TransformationType::ROTATION;
    t.vectorValue = rotationAxis;
    t.floatValue = rads;

    transformList.push_back(t);
}

void CSGNode::scale(vec3 factor)
{
    Transformation t;
    t.type = TransformationType::SCALE;
    t.vectorValue = factor;

    transformList.push_back(t);
}

void CSGNode::recursiveMark(int *_index)
{
	qDebug("MARKING NODE WITH ID = %d WITH INDEX = %d", id, *_index);

	index = *_index;

	leftIndex = -1;
	rightIndex = -1;

	if(leftChild)
	{
		*_index = *_index + 1;
		leftIndex = *_index;
		leftChild->recursiveMark(_index);
	}

	if(rightChild)
	{
		*_index = *_index + 1;
		rightIndex = *_index;
		rightChild->recursiveMark(_index);
	}
}

void CSGNode::toArray(std::vector<CSGNode> *nodes)
{
	if(leftChild)
	{
		nodes->push_back(*leftChild);
		leftChild->toArray(nodes);
	}

	if(rightChild)
	{
		nodes->push_back(*rightChild);
		rightChild->toArray(nodes);
	}
}

bool CSGNode::rayIntersection(vec3 origin, vec3 ray, int numPrimitives,  RayIntersection &intersection)
{
	return false;
}

bool CSGNode::rayIntersection(vec3 origin, vec3 ray, RayIntersection *intersections, int2 *arrayIndex)
{
	return false;
}

bool CSGNode::rayIntersection(vec3 origin, vec3 ray, RayIntersection &in, RayIntersection &out)
{
	return false;
}
