#include "csgtree.h"

CSGTree::CSGTree()
{
    numBoxes = 0;
    numCylinders = 0;
    numSpheres = 0;
    numUnions = 0;
    numDifferences = 0;
    numIntersections = 0;
	IDcount = 0;
}

int CSGTree::getTypeSize(NodeType _type)
{
    switch(_type)
    {
    case NodeType::BOX:
        return numBoxes+1;
    case NodeType::CYLINDER:
        return numCylinders+1;
    case NodeType::SPHERE:
        return numSpheres+1;
	case NodeType::OP_UNION:
        return numUnions+1;
	case NodeType::OP_DIFFERENCE:
        return numDifferences+1;
	case NodeType::OP_INTERSECTION:
        return numIntersections+1;
    default:
        return 0;
    }
}


void CSGTree::addNode(CSGNode *_node, bool hasID)
{
	if(hasID)
	{
		nodes.push_back(_node);
		IDcount = std::max(IDcount,_node->id);
		IDcount++;
	}
	else
	{
		_node->id = IDcount;
		if(_node->leftChild) _node->leftChild->parent = IDcount;
		if(_node->rightChild) _node->rightChild->parent = IDcount;
		nodes.push_back(_node);
		IDcount++;
	}

    switch(_node->type)
    {
	case NodeType::BOX:
        numBoxes++;
        break;
    case NodeType::CYLINDER:
        numCylinders++;
        break;
	case NodeType::SPHERE:
        numSpheres++;
        break;
	case NodeType::OP_UNION:
        numUnions++;
        break;
	case NodeType::OP_DIFFERENCE:
        numDifferences++;
        break;
	case NodeType::OP_INTERSECTION:
        numIntersections++;
        break;
    default:
        break;
    }
}

CSGNode* CSGTree::getNode(int id)
{
	if(id == -1)
		return NULL;

	for(int i=0; i<nodes.size(); i++)
	{
		if(nodes[i]->id == id)
			return nodes[i];
	}
	return NULL;
}

CSGNode* CSGTree::getRoot()
{
	for(int i=0; i<nodes.size(); i++)
	{
		if(nodes[i]->parent == -1)
			return nodes[i];
	}
}

int CSGTree::getNodeIndex(int id)
{
	for(int i=0; i<nodes.size(); i++)
	{
		if(nodes[i]->id == id)
			return i;
	}

	return -1;
}

int CSGTree::getPrimitiveCount()
{
	return numSpheres + numBoxes + numCylinders;
}


bool CSGTree::nameExists(QString _name)
{
	qDebug("Checking %d nodes", nodes.size());
    for(int i=0; i<nodes.size(); i++)
    {
        if(nodes[i]->name.compare(_name)==0)
            return true;
    }
    return false;

	nodes.clear();
	IDcount = 0;
}


void CSGTree::destruct()
{
	for(int i=0; i<nodes.size(); i++)
	{
		delete nodes[i];
	}
	numBoxes = 0;
	numCylinders = 0;
	numSpheres = 0;
	numUnions = 0;
	numDifferences = 0;
	numIntersections = 0;
	IDcount = 0;
	nodes.clear();

	qDebug("Tree content destructed");
}

