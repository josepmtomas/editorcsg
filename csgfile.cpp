#include "csgfile.h"

bool isValid(CSGNode *root)
{
	if(root->type == NodeType::OP_UNION ||
		root->type == NodeType::OP_DIFFERENCE ||
		root->type == NodeType::OP_INTERSECTION)
	{
		if(!root->leftChild)
			return false;
		if(!root->rightChild)
			return false;

		return (isValid(root->leftChild) && isValid(root->rightChild));
	}
	else
	{
		return true;
	}
}
