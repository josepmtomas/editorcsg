#ifndef CAMERA_H
#define CAMERA_H

#include "Algebra.h"

class Camera
{
private:
	vec3	u,v,w;

public:
	vec3	pov;
	vec3	poi;
	float	fov;

	Camera();

	void setPointOfView(vec3 _pov);
	void setPointOfInterest(vec3 _poi);
	void setFieldOfView(float _fov);

	void calculateUVW();

	vec3 visualPixel(float i, float j, int width, int height);
};

#endif // CAMERA_H
