#include "transformation.h"

Transformation::Transformation()
{
    type = TransformationType::NONE;
    vectorValue = vec3(0,0,0);
    floatValue = 0.0f;
}

QString Transformation::getListString()
{
    QString nameString;
    QString numberString;

    switch(type)
    {
        case TransformationType::TRANSLATION:
            nameString = QString("Translacion: (");
            nameString.append("x: ");
            numberString = QString::number(vectorValue.x, 'g', 4);
            nameString.append(numberString);
            nameString.append(" y: ");
            numberString = QString::number(vectorValue.y, 'g', 4);
            nameString.append(numberString);
            nameString.append(" z: ");
            numberString = QString::number(vectorValue.z, 'g', 4);
            nameString.append(numberString);
            nameString.append(")");

            return nameString;

        case TransformationType::ROTATION:
            nameString = QString("Rotacion: Eje (");
            nameString.append("x: ");
            numberString = QString::number(vectorValue.x, 'g', 4);
            nameString.append(numberString);
            nameString.append(" y: ");
            numberString = QString::number(vectorValue.y, 'g', 4);
            nameString.append(numberString);
            nameString.append(" z: ");
            numberString = QString::number(vectorValue.z, 'g', 4);
            nameString.append(numberString);
            nameString.append(")");

            nameString.append(" Angulo: (");
            numberString = QString::number(floatValue, 'g', 4);
            nameString.append(numberString);
            nameString.append(")");

            return nameString;

        case TransformationType::SCALE:
            nameString = QString("Escala: (");
            nameString.append("x: ");
            numberString = QString::number(vectorValue.x, 'g', 4);
            nameString.append(numberString);
            nameString.append(" y: ");
            numberString = QString::number(vectorValue.y, 'g', 4);
            nameString.append(numberString);
            nameString.append(" z: ");
            numberString = QString::number(vectorValue.z, 'g', 4);
            nameString.append(numberString);
            nameString.append(")");

            return nameString;

        default:
            return QString("NONE");
    }

}
