#ifndef DIALOGOPERATION_H
#define DIALOGOPERATION_H

#include <QDialog>
#include <QList>
#include <QMessageBox>
#include "csgtree.h"
#include "union.h"
#include "difference.h"
#include "intersection.h"
#include "dialogtransformation.h"

namespace Ui {
class DialogOperation;
}

class DialogOperation : public QDialog
{
    Q_OBJECT
    
public:

    CSGTree *tree;
    CSGNode *node;
    bool    *result;
	bool	editing;

    explicit DialogOperation(QWidget *parent = 0);
	DialogOperation(QWidget *parent, CSGTree *_tree, CSGNode *_operation, bool *_result, bool _editing);
    ~DialogOperation();

    void accept();
    
private slots:

    void on_buttonBox_accepted();
    void on_buttonBox_rejected();

    void on_addPushButton_clicked();
    void on_deletePushButton_clicked();
    void on_editPushButton_clicked();
    void on_upPushButton_clicked();
    void on_downPushButton_clicked();

private:
    Ui::DialogOperation *ui;

    void updateLists();
    void updateTransformationsList();
};

#endif // DIALOGOPERATION_H
