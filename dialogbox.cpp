#include "dialogbox.h"
#include "ui_dialogbox.h"

DialogBox::DialogBox(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogBox)
{
    ui->setupUi(this);

}

DialogBox::DialogBox(QWidget *parent, CSGTree *tree, Box *box, bool *result, bool editing):
    QDialog(parent),
    ui(new Ui::DialogBox)
{
    this->tree = tree;
    this->result = result;
    this->box = box;
    this->size = vec3(box->width, box->height, box->depth);
    this->editing = editing;
	this->edited = false;

    initialName = box->name;

    ui->setupUi(this);

    ui->nameLineEdit->setText(box->name);

    ui->widthSpinBox->setValue(size.x);
    ui->heightSpinBox->setValue(size.y);
    ui->depthSpinBox->setValue(size.z);

    ui->widthSpinBox->update();
    ui->heightSpinBox->update();
    ui->depthSpinBox->update();

    updateTransformationsList();

	if(editing)
	{
		this->setWindowTitle("Editar un cubo");
	}
	else
	{
		this->setWindowTitle("Crear un cubo");
	}
}

DialogBox::~DialogBox()
{
    delete ui;
}

void DialogBox::updateTransformationsList()
{
    ui->transformationsListWidget->clear();

    for(int i=0; i < box->transformList.size(); i++)
    {
        ui->transformationsListWidget->addItem(box->transformList[i].getListString());
    }
}

void DialogBox::accept()
{
    if(*result == true)
        DialogBox::close();
}

void DialogBox::on_heightSpinBox_valueChanged(double arg1)
{
    size.y = (float)arg1;
}

void DialogBox::on_widthSpinBox_valueChanged(double arg1)
{
    size.x = (float)arg1;
}

void DialogBox::on_depthSpinBox_valueChanged(double arg1)
{
    size.z = (float)arg1;
}


void DialogBox::on_buttonBox_accepted()
{
    if(editing)
    {
        if(ui->widthSpinBox->value() == 0.0 ||
           ui->heightSpinBox->value() == 0.0 ||
           ui->depthSpinBox->value() == 0.0)
        {
            QMessageBox::warning(this, "Editar un cubo", "El cubo no puede tener dimensiones 0");
        }
        else if(ui->nameLineEdit->text().isEmpty())
        {
            QMessageBox::warning(this, "Editar un cubo", "El cubo debe tener un nombre");
        }
        else if(ui->nameLineEdit->text().compare(initialName) != 0)
        {
            if(tree->nameExists(ui->nameLineEdit->text()))
            {
                QMessageBox::warning(this, "Editar un cubo", "El nombre ya existe");
            }
            else
            {
                *result = true;
                box->name = ui->nameLineEdit->text();
                box->rebuild(size);
                accept();
            }
        }
        else
        {
            *result = true;
            box->rebuild(size);
            accept();
        }
    }
    else
    {
        if(ui->widthSpinBox->value() == 0.0 ||
           ui->heightSpinBox->value() == 0.0 ||
           ui->depthSpinBox->value() == 0.0)
        {
            QMessageBox::warning(this, "Crear un cubo", "El cubo no puede tener dimensiones 0");
        }
        else if(box->name.isEmpty())
        {
            QMessageBox::warning(this, "Crear un cubo", "El cubo debe tener un nombre");
        }
        else if(tree->nameExists(box->name))
        {
            QMessageBox::warning(this, "Crear un cubo", "El nombre ya existe");
        }
        else
        {
            box->build(size);
            *result = true;
            accept();
        }
    }
}

void DialogBox::on_nameLineEdit_textChanged(const QString &arg1)
{
    //box->name = arg1;
}

void DialogBox::on_addButton_clicked()
{
    Transformation transformation;
    bool transformationResult = false;

    DialogTransformation dialog(0, &transformation, &transformationResult, false);
    dialog.setModal(true);
    dialog.exec();

    if(transformationResult == true)
    {
        box->transformList.push_back(transformation);
        updateTransformationsList();
    }
}

void DialogBox::on_deletePushButton_clicked()
{
    int index = ui->transformationsListWidget->currentRow();

    if(index >= 0)
    {
        box->transformList.erase(box->transformList.begin()+index);
        updateTransformationsList();
    }
}

void DialogBox::on_editPushButton_clicked()
{
    int index = ui->transformationsListWidget->currentRow();
    Transformation transformation;
    bool transformationResult = false;

    if(index >= 0)
    {
        transformation = box->transformList[index];

        DialogTransformation dialog(0, &transformation, &transformationResult, true);
        dialog.setModal(true);
        dialog.exec();

        if(transformationResult)
        {
            box->transformList[index] = transformation;
            updateTransformationsList();
        }
    }
}

void DialogBox::on_upPushButton_clicked()
{
    int index = ui->transformationsListWidget->currentRow();

    if(index >= 1)
    {
        std::swap(box->transformList[index], box->transformList[index-1]);
        updateTransformationsList();
        ui->transformationsListWidget->setCurrentRow(index-1);
    }
}

void DialogBox::on_downPushButton_clicked()
{
    int index = ui->transformationsListWidget->currentRow();

    if(index < box->transformList.size()-1)
    {
        std::swap(box->transformList[index], box->transformList[index+1]);
        updateTransformationsList();
        ui->transformationsListWidget->setCurrentRow(index+1);
    }
}
