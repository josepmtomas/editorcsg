#ifndef UNION_H
#define UNION_H

#include "csgnode.h"

class Union : public CSGNode
{
public:

    Union();

    QString getListString();
	QString getDescription();
    void propagateTransformations();
    void translation(vec3 amount);
    void rotation(vec3 rotationAxis, float rads);
    void scale(vec3 factor);

	bool rayIntersection(vec3 origin, vec3 ray, int numPrimitives, RayIntersection &intersection);
	bool rayIntersection(vec3 origin, vec3 ray, RayIntersection *intersections, int2 *arrayIndex);
	bool rayIntersection(vec3 origin, vec3 ray, RayIntersection &in, RayIntersection &out);
};

#endif // UNION_H
