#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>

namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit Dialog(QWidget *parent = 0);
    explicit Dialog(QWidget *parent, int *parameter);
    ~Dialog();
    
private:
    Ui::Dialog *ui;
    int *param;
};

#endif // DIALOG_H
