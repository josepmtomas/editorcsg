#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTreeWidgetItem>
#include <QStringList>
#include <QFileDialog>
#include <QColor>

#include <vector>

#include "dialogbox.h"
#include "dialogcylinder.h"
#include "dialogsphere.h"
#include "dialogoperation.h"
#include "dialogprogress.h"
#include "dialogrender.h"
#include "dialog.h"
#include "csgnode.h"
#include "csgtree.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:

	std::vector<QTreeWidgetItem*> errorItems;

    CSGTree tree;

    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

	void update();
    void updateNodeList();
	void updateTree();
	void rebuildTree();

	void checkTreeErrors();
	void checkTreeChildErrors(QTreeWidgetItem *parent, QTreeWidgetItem *item, bool left);

	void closeEvent(QCloseEvent *event);
    
private slots:

    void on_unionButton_clicked();
	void on_differenceButton_clicked();
	void on_intersectionButton_clicked();

    void on_sphereButton_clicked();
    void on_cylinderButton_clicked();
    void on_boxButton_clicked();

    void on_deletePushButton_clicked();
    void on_editPushButton_clicked();

	void on_treeWidget_itemClicked(QTreeWidgetItem *item, int column);

	void on_actionNew_triggered();
	void on_actionOpen_triggered();
	void on_actionSave_triggered();
	void on_actionSaveAs_triggered();
	void on_actionExport_triggered();
	void on_actionExit_triggered();

    void on_actionCPU_triggered();

	void on_errorsListWidget_clicked(const QModelIndex &index);

	void on_upPushButton_clicked();

	void on_downPushButton_clicked();

private:
    Ui::MainWindow *ui;

	QString	currentFile;
	bool	changesMade;

	void addTreeRoots();
	void addTreeChild(QTreeWidgetItem *parent, int id);

	void updateTreeRoots();
	void updateTreeChild(QTreeWidgetItem *item);

	void saveExport(QString filename);
};

#endif // MAINWINDOW_H
