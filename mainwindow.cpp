#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
	tree = CSGTree();

	QStringList columnNames;
	columnNames << "Nodo" << "Descripción" << "ID";

	ui->setupUi(this);
	ui->treeWidget->setHeaderLabels(columnNames);
	ui->treeWidget->setHeaderHidden(false);
	ui->treeWidget->setColumnHidden(2,true);
	//ui->treeWidget->setRootIsDecorated(true);
	ui->treeWidget->setColumnWidth(0,200);

	setWindowTitle("Editor CSG - Nuevo archivo");

	changesMade = false;
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::update()
{
	updateNodeList();
	updateTree();
}

void MainWindow::updateNodeList()
{
    ui->listWidget->clear();

	qDebug("\nUPDATING(ui->listWidget) LIST_SIZE = %d", tree.nodes.size());
    for(int i=0; i<tree.nodes.size(); i++)
    {
        ui->listWidget->addItem(tree.nodes[i]->getListString());
		qDebug("\t[%d] ADDED NODE WITH ID = %d", i, tree.nodes[i]->id);
    }
}

void MainWindow::updateTree()
{
	updateTreeRoots();
}

void MainWindow::rebuildTree()
{
	ui->treeWidget->clear();
	addTreeRoots();
	ui->treeWidget->expandAll();
}

void MainWindow::checkTreeErrors()
{
	QTreeWidgetItem *item;
	CSGNode *node;
	QString errorString;
	int id;

	ui->errorsListWidget->clear();
	errorItems.clear();

	qDebug("\nCHECKING TREE ERRORS");
	qDebug("--------------------");

	if(ui->treeWidget->topLevelItemCount() == 0)
	{
		ui->errorsListWidget->addItem("El árbol está vacío");
		ui->errorsListWidget->item(ui->errorsListWidget->count()-1)->setTextColor(QColor(255,0,0));
		errorItems.push_back(NULL);
	}
	if(ui->treeWidget->topLevelItemCount() > 1)
	{
		ui->errorsListWidget->addItem("El árbol tiene más de una raiz");
		ui->errorsListWidget->item(ui->errorsListWidget->count()-1)->setTextColor(QColor(255,0,0));
		errorItems.push_back(NULL);
	}

	for(int i=0; i<ui->treeWidget->topLevelItemCount(); i++)
	{
		errorString.clear();
		item = ui->treeWidget->topLevelItem(i);
		id = item->text(2).toInt();
		node = tree.getNode(id);
		qDebug("Checking node ID = %d", id);

		if(node->type == NodeType::OP_UNION ||
			node->type == NodeType::OP_DIFFERENCE ||
			node->type == NodeType::OP_INTERSECTION)
		{
			qDebug("\tnode is operation");
			checkTreeChildErrors(item, item->child(0), true);
			checkTreeChildErrors(item, item->child(1), false);
		}
		else
		{
			qDebug("\tnode is primitive");
			errorItems.push_back(item);

			errorString.append("La primitiva (");
			errorString.append(node->name);
			errorString.append(") no está asignada a ninguna operación");
			ui->errorsListWidget->addItem(errorString);
			ui->errorsListWidget->item(ui->errorsListWidget->count()-1)->setTextColor(QColor(255,0,0));
		}
	}

	if(errorItems.size() == 0)
	{
		errorString = QString("Problemas (0)");
		ui->treeTabWidget->setTabText(1,errorString);
	}
	else
	{
		errorString = QString("Problemas (");
		errorString.append(QString::number(errorItems.size()));
		errorString.append(")");
		ui->treeTabWidget->setTabText(1,errorString);
	}
}

void MainWindow::checkTreeChildErrors(QTreeWidgetItem *parent, QTreeWidgetItem *item, bool left)
{
	CSGNode *node;
	QString errorString;
	int id;

	id = item->text(2).toInt();
	node = tree.getNode(id);

	qDebug("\tChecking child (%s)", item->text(0).toUtf8());

	if(item->text(0).compare("NONE") == 0)
	{
		errorItems.push_back(parent);

		errorString.append("El nodo (");
		errorString.append(parent->text(0));
		if(left)
			errorString.append(") no tiene un hijo izquierdo asignado.");
		else
			errorString.append(") no tiene un hijo derecho asignado");

		ui->errorsListWidget->addItem(errorString);
		ui->errorsListWidget->item(ui->errorsListWidget->count()-1)->setTextColor(QColor(255,0,0));
	}
	else
	{
		if(node->type == NodeType::OP_UNION ||
			node->type == NodeType::OP_DIFFERENCE ||
			node->type == NodeType::OP_INTERSECTION)
		{
			checkTreeChildErrors(item, item->child(0), true);
			checkTreeChildErrors(item, item->child(1), true);
		}
	}
}

void MainWindow::closeEvent(QCloseEvent *event)
{
	if(changesMade)
	{
		QMessageBox::StandardButton resBtn = QMessageBox::question( this, "Salir","Guardar cambios antes de salir?",
																		QMessageBox::Cancel | QMessageBox::No | QMessageBox::Yes,
																		QMessageBox::Yes);

		if (resBtn != QMessageBox::Yes) {
			if(resBtn == QMessageBox::No)
			{
				tree.destruct();
				event->accept();
			}
			else //if(resBtn == QMessageBox::Cancel)
			{
				event->ignore();
			}
		} else {
			tree.destruct();
			event->accept();
		}
	}
	else
	{
		event->accept();
	}
}

void MainWindow::on_unionButton_clicked()
{
    Union   *operation = new Union();
    bool    result = false;

    QString nameString;
    QString numberString;

	numberString.setNum(tree.getTypeSize(NodeType::OP_UNION));
    nameString.append("Union");
    nameString.append(numberString);
    operation->name = nameString;

	DialogOperation dialog(0,&tree,operation,&result, false);
    dialog.setModal(true);
    dialog.exec();

	if(result)
	{
		tree.addNode(operation,false);
		updateNodeList();
		rebuildTree();
		checkTreeErrors();
	}
	else
	{
		delete operation;
	}
}

void MainWindow::on_differenceButton_clicked()
{
	Difference   *operation = new Difference();
	bool    result = false;

	QString nameString;
	QString numberString;

	numberString.setNum(tree.getTypeSize(NodeType::OP_DIFFERENCE));
	nameString.append("Difference");
	nameString.append(numberString);
	operation->name = nameString;

	DialogOperation dialog(0,&tree,operation,&result, false);
	dialog.setModal(true);
	dialog.exec();

	if(result)
	{
		tree.addNode(operation,false);
		updateNodeList();
		rebuildTree();
		checkTreeErrors();
	}
	else
	{
		delete operation;
	}
}

void MainWindow::on_intersectionButton_clicked()
{
	Intersection  *operation = new Intersection();
	bool    result = false;

	QString nameString;
	QString numberString;

	numberString.setNum(tree.getTypeSize(NodeType::OP_INTERSECTION));
	nameString.append("Intersection");
	nameString.append(numberString);
	operation->name = nameString;

	DialogOperation dialog(0,&tree,operation,&result, false);
	dialog.setModal(true);
	dialog.exec();

	if(result)
	{
		tree.addNode(operation,false);
		updateNodeList();
		rebuildTree();
		checkTreeErrors();
	}
	else
	{
		delete operation;
	}
}

void MainWindow::on_sphereButton_clicked()
{
    Sphere  *sphere = new Sphere();
    bool    result = false;

    QString nameString;
    QString numberString;

    numberString.setNum(tree.getTypeSize(NodeType::SPHERE));
    nameString.append("Sphere");
    nameString.append(numberString);
    sphere->name = nameString;

	DialogSphere dialog(0,&tree, sphere, &result, false);
    dialog.setModal(true);
    dialog.exec();

    if(result)
    {
        printf("Should add a sphere.\n");
		tree.addNode(sphere,false);

		updateNodeList();
		rebuildTree();
		checkTreeErrors();
    }
    else
    {
        delete sphere;
        printf("Should not add a sphere.\n");
    }
    fflush(stdout);
}

void MainWindow::on_cylinderButton_clicked()
{
    Cylinder    *cylinder = new Cylinder();
    bool        result = false;

    QString     nameString;
    QString     numberString;

    numberString.setNum(tree.getTypeSize(NodeType::CYLINDER));
    nameString.append("Cylinder");
    nameString.append(numberString);
    cylinder->name = nameString;

    DialogCylinder dialog(0,&tree, cylinder, &result, false);
    dialog.setModal(true);
    dialog.exec();

    if(result){
        printf("Should create cylinder.\n");
		tree.addNode(cylinder,false);
		updateNodeList();
		rebuildTree();
		checkTreeErrors();
    }
    else
    {
        delete cylinder;
        printf("Should not create cylinder.\n");
    }
    fflush(stdout);
}

void MainWindow::on_boxButton_clicked()
{
    Box     *box = new Box();
    bool    result = false;

    QString     nameString;
    QString     numberString;

    numberString.setNum(tree.getTypeSize(NodeType::BOX));
    nameString.append("Box");
    nameString.append(numberString);
    box->name = nameString;

    DialogBox dialog(0,&tree, box, &result, false);
    dialog.setModal(true);
    dialog.exec();

    if(result){
		tree.addNode(box,false);
		updateNodeList();
		rebuildTree();
		checkTreeErrors();
    }
    else
    {
        delete box;
    }
}

void MainWindow::on_deletePushButton_clicked()
{
	int index = ui->listWidget->currentRow();
	CSGNode *node;
	CSGNode *parentNode;

	if(index >= 0)
	{
		node = tree.nodes[index];
		qDebug("Deleting node ID = %d", node->id);

		if(node->parent >= 0)
		{
			parentNode = tree.getNode(node->parent);
			qDebug("\tPARENT IS %d", parentNode->id);

			if(parentNode->leftChild->id == node->id)
			{
				parentNode->leftChild = NULL;
				qDebug("\tNODE IS LEFT CHILD");
			}
			else
			{
				parentNode->rightChild = NULL;
				qDebug("\tNODE IS RIGHT CHILD");
			}

			if(node->type == NodeType::OP_UNION ||
			   node->type == NodeType::OP_DIFFERENCE ||
			   node->type == NodeType::OP_INTERSECTION)
			{
				qDebug("\tNODE IS OPERATION");
				if(node->leftChild)
					node->leftChild->parent = -1;
				if(node->rightChild)
					node->rightChild->parent = -1;
			}

		}
		else
		{
			if(node->leftChild)
				node->leftChild->parent = -1;
			if(node->rightChild)
				node->rightChild->parent = -1;


		}

		tree.nodes.erase(tree.nodes.begin()+index);
		updateNodeList();
		rebuildTree();
		checkTreeErrors();


		/*if(tree.nodes[index]->type == NodeType::OP_UNION ||
			tree.nodes[index]->type == NodeType::OP_DIFFERENCE ||
			tree.nodes[index]->type == NodeType::OP_INTERSECTION)
		{
			tree.nodes[index]->leftChild->parent = -1;
			tree.nodes[index]->rightChild->parent = -1;

			if(tree.getNode(tree.nodes[index]->id)->leftChild->id == tree.nodes[index]->id)
				tree.getNode(tree.nodes[index]->id)->leftChild = NULL;
			if(tree.getNode(tree.nodes[index]->id)->rightChild->id == tree.nodes[index]->id)
				tree.getNode(tree.nodes[index]->id)->rightChild = NULL;
		}
		else
		{
			if(tree.getNode(tree.nodes[index]->parent)->leftChild->id == tree.nodes[index]->id)
			{
				tree.getNode
			}
		}

		tree.nodes.erase(tree.nodes.begin()+index);
		updateNodeList();
		rebuildTree();*/
	}
}

void MainWindow::on_editPushButton_clicked()
{
    int index = ui->listWidget->currentRow();
    //printf("Selected index: %d\n", index);
    //fflush(stdout);

    NodeType type;
    bool result = false;

    if(index >= 0)
    {
        type = tree.nodes[index]->type;
    }
    else
    {
        type = NodeType::BLANK;
    }

	if(type == NodeType::OP_UNION)
	{
		Union *operation = (Union*)tree.nodes[index];

		DialogOperation dialog(0, &tree, operation, &result, true);
		dialog.setModal(true);
		dialog.exec();

		updateNodeList();
		rebuildTree();
		checkTreeErrors();
	}
	else if(type == NodeType::OP_DIFFERENCE)
	{
		Difference *operation = (Difference*)tree.nodes[index];

		DialogOperation dialog(0, &tree, operation, &result, true);
		dialog.setModal(true);
		dialog.exec();

		updateNodeList();
		rebuildTree();
		checkTreeErrors();
	}
	else if(type == NodeType::OP_INTERSECTION)
	{
		Difference *operation = (Difference*)tree.nodes[index];

		DialogOperation dialog(0, &tree, operation, &result, true);
		dialog.setModal(true);
		dialog.exec();

		updateNodeList();
		rebuildTree();
		checkTreeErrors();
	}
	else if(type == NodeType::BOX)
	{
		Box *box = (Box*)tree.nodes[index];

		DialogBox dialog(0, &tree, box, &result, true);
		dialog.setModal(true);
		dialog.exec();

		update();
		checkTreeErrors();
	}
	else if(type == NodeType::CYLINDER)
    {
        Cylinder *cylinder = (Cylinder*)tree.nodes[index];

		if(cylinder->pivotOnCenter)
			qDebug("PIVOT ON CENTER");
		else
			qDebug("PIVOT ON BASE");

        DialogCylinder dialog(0,&tree,cylinder,&result, true);
        dialog.setModal(true);
        dialog.exec();

		update();
		checkTreeErrors();
    }
	else if(type == NodeType::SPHERE)
	{
		Sphere *sphere = (Sphere*)tree.nodes[index];

		DialogSphere dialog(0,&tree,sphere,&result, true);
		dialog.setModal(true);
		dialog.exec();

		update();
		checkTreeErrors();
	}
}

void MainWindow::on_actionNew_triggered()
{
	setWindowTitle("Editor CSG - Nuevo archivo");

	tree.destruct();

	currentFile.clear();
	changesMade = false;

	updateNodeList();
	rebuildTree();
}

void MainWindow::on_actionOpen_triggered()
{
	QString filename = QFileDialog::getOpenFileName(
				this,
				"Abrir archivo",
				"",
				"CSGEditor file (*.csg);;All files(*.*)");

	if(!filename.isEmpty())
	{
		tree.destruct();

		DialogProgress dialog(this, filename, &tree, false);
		dialog.setModal(true);
		dialog.exec();

		currentFile = filename;

		filename = QString("Editor CSG - ");
		filename.append(currentFile);
		setWindowTitle(filename);

		updateNodeList();
		rebuildTree();
		checkTreeErrors();
	}
}

void MainWindow::on_actionSave_triggered()
{
	QString filename;

	if(currentFile.isEmpty())
	{
		on_actionSaveAs_triggered();
	}
	else
	{
		DialogProgress dialog(this, currentFile, &tree, true);
		dialog.setModal(true);
		dialog.exec();

		filename = QString("Editor CSG - ");
		filename.append(currentFile);
		setWindowTitle(filename);
	}

	qDebug(currentFile.toUtf8());
}

void MainWindow::on_actionSaveAs_triggered()
{
	QString filename = QFileDialog::getSaveFileName(
				this,
				"Guardar archivo como",
				"",
				"CSGEditor file (*.csg);;All files(*.*)"
				);

	if(!filename.isEmpty())
	{
		DialogProgress dialog(this, filename, &tree, true);
		dialog.setModal(true);
		dialog.exec();

		currentFile = filename;

		filename = QString("Editor CSG - ");
		filename.append(currentFile);
		setWindowTitle(filename);
	}
}


void MainWindow::on_actionExport_triggered()
{
	if(errorItems.size() > 0)
	{
		QMessageBox::warning(this,"No se puede exportar","El árbol no es válido");
	}
	else
	{
		tree.getRoot()->propagateTransformations();

		QString filename = QFileDialog::getSaveFileName(
					this,
					"Exportar archivo",
					"",
					"CUDA RayTracer CSG file (*.csg);;All files(*.*)");

		if(!filename.isEmpty())
		{
			saveExport(filename);
		}
	}
}

void MainWindow::on_actionExit_triggered()
{
	QMainWindow::close();
}

void MainWindow::on_actionCPU_triggered()
{
	QString filename;



	if(currentFile.isEmpty())
	{

	}
	else
	{
		on_actionSave_triggered();

		if(errorItems.size() == 0)
	   {
		   tree.getRoot()->propagateTransformations();

		   DialogRender dialog(this, tree.getRoot(), tree.getPrimitiveCount());
		   dialog.setModal(true);
		   dialog.exec();

		   tree.destruct();

		   DialogProgress dialogProgress(this, currentFile, &tree, false);
		   dialogProgress.setModal(true);
		   dialogProgress.exec();

		   filename = QString("Editor CSG - ");
		   filename.append(currentFile);
		   setWindowTitle(filename);

		   updateNodeList();
		   rebuildTree();
		   checkTreeErrors();
	   }
	   else
	   {
		   QMessageBox::warning(this,"No se puede visualizar","El árbol ha der ser válido para visualizarse");
	   }
	}
}

void MainWindow::addTreeRoots()
{
	QTreeWidgetItem *item;
	CSGNode *node;
	int id;

	fprintf(stdout,"Adding %d node(s)\n",tree.nodes.size());
	for(int i=0; i<tree.nodes.size(); i++)
	{
		fprintf(stdout,"Adding node [%i] with ID = %d\n", i, tree.nodes[i]->id);
		fprintf(stdout,"\tPARENT = %d\n", tree.nodes[i]->parent);
		fprintf(stdout,"\tNAME = '%s'\n", tree.nodes[i]->name.toStdString().c_str());
		if(tree.nodes[i]->parent == -1)
		{
			item = new QTreeWidgetItem(ui->treeWidget);
			id = tree.nodes[i]->id;
			node = tree.getNode(id);

			item->setText(0, node->name);
			item->setText(1, node->getDescription());
			item->setText(2, QString::number(id));
			ui->treeWidget->addTopLevelItem(item);

			if(node->type == NodeType::OP_UNION ||
					node->type == NodeType::OP_DIFFERENCE ||
					node->type == NodeType::OP_INTERSECTION)
			{
				if(node->leftChild)
					addTreeChild(item, node->leftChild->id);
				else
					addTreeChild(item, -1);

				if(node->rightChild)
					addTreeChild(item, node->rightChild->id);
				else
					addTreeChild(item, -1);
			}
		}
	}
	fflush(stdout);
}

void MainWindow::addTreeChild(QTreeWidgetItem *parent, int id)
{
	QTreeWidgetItem *item = new QTreeWidgetItem(parent);
	CSGNode *node;

	if(id >= 0)
	{
		node = tree.getNode(id);
		item->setTextColor(0,QColor(1,0,0));
		item->setText(0, node->name);
		item->setText(1, node->getDescription());
		item->setText(2, QString::number(id));

		parent->addChild(item);

		if(node->type == NodeType::OP_UNION ||
			node->type == NodeType::OP_DIFFERENCE ||
			node->type == NodeType::OP_INTERSECTION)
		{
			if(node->leftChild)
				addTreeChild(item, node->leftChild->id);
			else
				addTreeChild(item, -1);

			if(node->rightChild)
				addTreeChild(item, node->rightChild->id);
			else
				addTreeChild(item, -1);
		}
	}
	else
	{
		item->setTextColor(0,QColor(255,0,0));
		item->setText(0,"NONE");
		item->setText(1,"");
		item->setText(2,QString::number(-1));

		parent->addChild(item);
	}
}

void MainWindow::on_treeWidget_itemClicked(QTreeWidgetItem *item, int column)
{
	int index;

	if(item)
	{
		index = tree.getNodeIndex(item->text(2).toInt());
		ui->listWidget->setCurrentRow(index);
	}
	//ui->listWidget->setCurrentRow(item->text(2).toInt());
}

void MainWindow::updateTreeRoots()
{
	QTreeWidgetItem *item;
	CSGNode *node;
	int id;

	for(int i=0; i<ui->treeWidget->topLevelItemCount(); i++)
	{
		item = ui->treeWidget->topLevelItem(i);
		id = item->text(2).toInt();
		node = tree.getNode(id);

		item->setText(0,node->name);
		item->setText(1,node->getDescription());

		if(node->type == NodeType::OP_UNION ||
			node->type == NodeType::OP_DIFFERENCE ||
			node->type == NodeType::OP_INTERSECTION)
		{
			updateTreeChild(item->child(0));
			updateTreeChild(item->child(1));
		}
	}
}

void MainWindow::updateTreeChild(QTreeWidgetItem *item)
{
	int id = item->text(2).toInt();
	CSGNode *node = tree.getNode(id);

	item->setText(0,node->name);
	item->setText(1,node->getDescription());

	if(node->type == NodeType::OP_UNION ||
		node->type == NodeType::OP_DIFFERENCE ||
		node->type == NodeType::OP_INTERSECTION)
	{
		updateTreeChild(item->child(0));
		updateTreeChild(item->child(1));
	}
}

void MainWindow::on_errorsListWidget_clicked(const QModelIndex &index)
{
	//qDebug(QString::number(ui->errorsListWidget->currentRow()).toUtf8());
	on_treeWidget_itemClicked(errorItems[ui->errorsListWidget->currentRow()],0);
}

void MainWindow::on_upPushButton_clicked()
{
	int index = ui->listWidget->currentRow();

	if((unsigned int)index >= 1)
	{
		std::swap(tree.nodes[index],tree.nodes[index-1]);
		updateNodeList();
		ui->listWidget->setCurrentRow(index-1);
	}
}

void MainWindow::on_downPushButton_clicked()
{
	int index = ui->listWidget->currentRow();

	if((unsigned int)index < tree.nodes.size()-1)
	{
		std::swap(tree.nodes[index], tree.nodes[index+1]);
		updateNodeList();
		ui->listWidget->setCurrentRow(index+1);
	}
}

void MainWindow::saveExport(QString filename)
{
	std::vector<CSGNode> nodeArray;
	std::vector<CSGNode> operations;
	std::vector<Box> boxes;
	std::vector<Cylinder> cylinders;
	std::vector<Sphere> spheres;

	int index = 0;
	int k;
	CSGNode *root = tree.getRoot();

	qDebug("EXPORTING CURRENT TREE");
	qDebug("----------------------");

	root->recursiveMark(&index);
	nodeArray.push_back(*root);
	root->toArray(&nodeArray);

	/*for(int i=0; i<nodeArray.size(); i++)
	{
		qDebug("[%d] INDEX = %d | ID = %d", i, nodeArray[i].index, nodeArray[i].id);
	}*/

	qDebug("----------------------");

	qDebug("OBTAINING LIST OF BOXES");
	k = 0;
	for(int i=0; i<nodeArray.size(); i++)
	{
		if(nodeArray[i].type == NodeType::BOX)
		{
			Box *box = (Box*)tree.getNode(nodeArray[i].id);
			boxes.push_back(*box);
			boxes[k].index = k;
			tree.getNode(nodeArray[i].id)->index = boxes.size()-1;
			qDebug("\tBOX ID = %d | INDEX = %d", boxes[k].id, boxes[k].index);
			k++;
		}
	}

	qDebug("OBTAINING LIST OF CYLINDERS");
	k = 0;
	for(int i=0; i<nodeArray.size(); i++)
	{
		if(nodeArray[i].type == NodeType::CYLINDER)
		{
			Cylinder *cylinder = (Cylinder*)tree.getNode(nodeArray[i].id);
			cylinders.push_back(*cylinder);
			cylinders[k].index = k;
			cylinder->index = k;
			qDebug("\tCYLINDER ID = %d | INDEX = %d", cylinders[k].id, cylinders[k].index);
			k++;
		}
	}

	qDebug("OBTAINING LIST OF SPHERES");
	k=0;
	for(int i=0; i<nodeArray.size(); i++)
	{
		if(nodeArray[i].type == NodeType::SPHERE)
		{
			Sphere *sphere = (Sphere*)tree.getNode(nodeArray[i].id);
			spheres.push_back(*sphere);
			spheres[k].index = k;
			sphere->index = k;
			qDebug("\tSPHERE ID = %d | INDEX = %d", spheres[k].id, spheres[k].index);
			k++;
		}
	}

	qDebug("OBTAINING LIST OF OPERATIONS");
	k=0;
	for(int i=0; i<nodeArray.size(); i++)
	{
		if(	nodeArray[i].type == NodeType::OP_UNION ||
			nodeArray[i].type == NodeType::OP_DIFFERENCE ||
			nodeArray[i].type == NodeType::OP_INTERSECTION)
		{
			CSGNode *op = tree.getNode(nodeArray[i].id);
			operations.push_back(*op);
			operations[k].index = k;
			op->index = k;
			qDebug("\tOPERATION ID = %d | INDEX = %d", operations[k].id, operations[k].index);
			k++;
		}
	}

	qDebug("----------------------");
	qDebug("WRITING FILE");

	FILE *file = fopen(filename.toUtf8().data(), "w");

	if(!file)
	{
		qDebug("Can't open the file to export");
	}
	else
	{
		/*fprintf(file,"CSGTree\n{\n");
		fprintf(file,"\tBoxes %d\n", tree.numBoxes);
		fprintf(file,"\tCylinders %d\n", tree.numCylinders);
		fprintf(file,"\tSpheres %d\n", tree.numSpheres);
		fprintf(file,"\tOperations %d\n", tree.numUnions + tree.numDifferences + tree.numIntersections);
		fprintf(file,"}\n\n");*/

		for(int i=0; i<boxes.size(); i++)
		{
			fprintf(file, "Box\n{\n");
			fprintf(file, "\tSize %.4f %.4f %.4f\n", boxes[i].width, boxes[i].height, boxes[i].depth);

			for(int j=0; j<boxes[i].transformList.size(); j++)
			{
				Transformation t = boxes[i].transformList[j];
				if(t.type == TransformationType::TRANSLATION)
					fprintf(file, "\tTranslation %.4f %.4f %.4f\n", t.vectorValue.x, t.vectorValue.y, t.vectorValue.z);
				else if(t.type == TransformationType::ROTATION)
					fprintf(file, "\tRotation %.4f %.4f %.4f %.4f\n", t.vectorValue.x, t.vectorValue.y, t.vectorValue.z, t.floatValue);
				else if(t.type == TransformationType::SCALE)
					fprintf(file, "\tScale %.4f %.4f %.4f\n", t.vectorValue.x, t.vectorValue.y, t.vectorValue.z);
			}
			fprintf(file, "}\n\n");
		}

		for(int i=0; i<cylinders.size(); i++)
		{
			fprintf(file, "Cylinder\n{\n");
			fprintf(file, "\tBase %.4f %.4f %.4f\n", cylinders[i].base.x, cylinders[i].base.y, cylinders[i].base.z);
			fprintf(file, "\tAxis %.4f %.4f %.4f\n", cylinders[i].axis.x, cylinders[i].axis.y, cylinders[i].axis.z);
			fprintf(file, "\tRadius %.4f\n", cylinders[i].radius);
			fprintf(file, "\tHeight %.4f\n", cylinders[i].height);

			/*for(int j=0; j<cylinders[i].transformList.size(); j++)
			{
				Transformation t = cylinders[i].transformList[j];
				if(t.type == TransformationType::TRANSLATION)
					fprintf(file, "\tTranslation %.4f %.4f %.4f\n", t.vectorValue.x, t.vectorValue.y, t.vectorValue.z);
				else if(t.type == TransformationType::ROTATION)
					fprintf(file, "\tRotation %.4f %.4f %.4f %.4f\n", t.vectorValue.x, t.vectorValue.y, t.vectorValue.z, t.floatValue);
				else if(t.type == TransformationType::SCALE)
					fprintf(file, "\tScale %.4f %.4f %.4f\n", t.vectorValue.x, t.vectorValue.y, t.vectorValue.z);
			}*/
			fprintf(file, "}\n\n");
		}

		for(int i=0; i<spheres.size(); i++)
		{
			fprintf(file, "Sphere\n{\n");
			fprintf(file, "\tCenter %.4f %.4f %.4f\n", spheres[i].center.x, spheres[i].center.y, spheres[i].center.z);
			fprintf(file, "\tRadius %.4f\n", spheres[i].radius);

			/*for(int j=0; j<spheres[i].transformList.size(); j++)
			{
				Transformation t = spheres[i].transformList[j];
				if(t.type == TransformationType::TRANSLATION)
					fprintf(file, "\tTranslation %.4f %.4f %.4f\n", t.vectorValue.x, t.vectorValue.y, t.vectorValue.z);
				else if(t.type == TransformationType::ROTATION)
					fprintf(file, "\tRotation %.4f %.4f %.4f %.4f\n", t.vectorValue.x, t.vectorValue.y, t.vectorValue.z, t.floatValue);
				else if(t.type == TransformationType::SCALE)
					fprintf(file, "\tScale %.4f %.4f %.4f\n", t.vectorValue.x, t.vectorValue.y, t.vectorValue.z);
			}*/
			fprintf(file, "}\n\n");
		}

		for(int i=0; i<operations.size(); i++)
		{
			if(operations[i].type == NodeType::OP_UNION)
				fprintf(file, "Union\n{\n");
			else if(operations[i].type == NodeType::OP_DIFFERENCE)
				fprintf(file, "Difference\n{\n");
			else if(operations[i].type == NodeType::OP_INTERSECTION)
				fprintf(file, "Intersection\n{\n");

			// LEFT
			int type = operations[i].leftChild->type;
			fprintf(file, "\tLeftType ");
			if(type == NodeType::BOX)
				fprintf(file, "%d\n", OBJECT_TYPE_CSG_BOX);
			else if(type == NodeType::CYLINDER)
				fprintf(file, "%d\n", OBJECT_TYPE_CSG_CYLINDER);
			else if(type == NodeType::SPHERE)
				fprintf(file, "%d\n", OBJECT_TYPE_CSG_SPHERE);
			else
				fprintf(file, "%d\n", OBJECT_TYPE_CSG_NODE);
			fprintf(file, "\tLeftIndex %d\n", operations[i].leftChild->index);

			// RIGHT
			type = operations[i].rightChild->type;
			fprintf(file, "\tRightType ");
			if(type == NodeType::BOX)
				fprintf(file, "%d\n", OBJECT_TYPE_CSG_BOX);
			else if(type == NodeType::CYLINDER)
				fprintf(file, "%d\n", OBJECT_TYPE_CSG_CYLINDER);
			else if(type == NodeType::SPHERE)
				fprintf(file, "%d\n", OBJECT_TYPE_CSG_SPHERE);
			else
				fprintf(file, "%d\n", OBJECT_TYPE_CSG_NODE);
			fprintf(file, "\tRightIndex %d\n", operations[i].rightChild->index);

			fprintf(file, "}\n\n");
		}

		fclose(file);
	}
}
